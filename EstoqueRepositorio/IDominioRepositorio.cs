﻿using EstoqueModelo;
using System.Collections.Generic;

namespace EstoqueRepositorio
{
    public interface IDominioRepositorio : IRepositorio<Dominio>
    {
        List<Dominio> obterPorTipo(int tipo);
        List<Dominio> obterPorTipoValor(int tipo, int valor);
        List<Dominio> obterPorTipoDescricao(int tipo, string descricao);
    }
}