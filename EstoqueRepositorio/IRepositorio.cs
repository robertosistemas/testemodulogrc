﻿using System;
using System.Collections.Generic;

namespace EstoqueRepositorio
{
    public interface IRepositorio<T> : IDisposable
    {
        void incluir(T entidade);
        void atualizar(T entidade);
        void excluir(int id);
        T obterPorId(int id);
        List<T> obterTodos();
    }
}