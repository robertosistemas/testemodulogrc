﻿using EstoqueModelo;
using System.Collections.Generic;

namespace EstoqueRepositorio
{
    public interface IPrecoPropostoRepositorio : IRepositorio<PrecoProposto>
    {
        List<PrecoProposto> obterPorBicicletaId(int bicicletaId);
        List<PrecoProposto> obterPorGerenteEstoqueId(int gerenteEstoqueId);
        List<PrecoProposto> obterPorGerenteLojaOnlineId(int gerenteLojaOnlineId);
        List<PrecoProposto> obterRegistroAgendar();
        List<PrecoProposto> obterRegistroProcessar();
    }
}