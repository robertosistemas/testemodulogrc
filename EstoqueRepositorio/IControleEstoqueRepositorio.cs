﻿using EstoqueModelo;
using System;
using System.Collections.Generic;

namespace EstoqueRepositorio
{
    public interface IControleEstoqueRepositorio : IRepositorio<ControleEstoque>
    {
        List<ControleEstoque> obterPorBicicletaId(int bicicletaId);
        List<ControleEstoque> obterPorPeriodo(DateTime dataInicio, DateTime dataFim);
        List<ControleEstoque> obterPorBicicletaIdPeriodo(int bicicletaId, DateTime dataInicio, DateTime dataFim);
    }
}