﻿using EstoqueModelo;
using System.Collections.Generic;

namespace EstoqueRepositorio
{
    public interface IGerenteRepositorio : IRepositorio<Gerente>
    {
        List<Gerente> obterPorTipoGerenteId(int tipoGerenteId);
        List<Gerente> obterPorNome(string nome);
    }
}