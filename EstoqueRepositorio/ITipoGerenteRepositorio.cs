﻿using EstoqueModelo;

namespace EstoqueRepositorio
{
    public interface ITipoGerenteRepositorio : IRepositorio<TipoGerente>
    {
    }
}