﻿using EstoqueModelo;
using System;
using System.Collections.Generic;

namespace EstoqueRepositorio
{
    public interface IBicicletaRepositorio : IRepositorio<Bicicleta>
    {
        List<Bicicleta> obterPorModelo(string modelo);
        void atualizarPreco(int id, Decimal novoPreco);
        void atualizarQuantidadeEstoque(int id, int novaQuantidade);
    }
}