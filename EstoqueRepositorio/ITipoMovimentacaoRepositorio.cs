﻿using EstoqueModelo;

namespace EstoqueRepositorio
{
    public interface ITipoMovimentacaoRepositorio : IRepositorio<TipoMovimentacao>
    {
    }
}