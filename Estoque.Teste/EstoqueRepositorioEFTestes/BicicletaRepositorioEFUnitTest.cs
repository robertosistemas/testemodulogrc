﻿using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Estoque.Teste.EstoqueRepositorioEFTestes
{

    [TestClass]
    public class BicicletaRepositorioEFUnitTest
    {

        [TestMethod]
        public void incluir()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {
                    using (IBicicletaRepositorio repos = new BicicletaRepositorioEF(ctx))
                    {
                        Bicicleta entidade = new Bicicleta { Modelo = "Caloi 400 Aro 26", Preco = 987, QuantidadeEstoque = 1 };

                        repos.incluir(entidade);
                        Assert.IsTrue(entidade.Id > 0);

                    }

                    tran.Commit();

                }
            }
        }


        [TestMethod]
        public void atualizar()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {
                    using (IBicicletaRepositorio repos = new BicicletaRepositorioEF(ctx))
                    {
                        Bicicleta entidade = new Bicicleta { Modelo = "Caloi 400 Aro 26", Preco = 987, QuantidadeEstoque = 1 };

                        repos.incluir(entidade);

                        entidade.Modelo = "<Teste de Alteração>";

                        repos.atualizar(entidade);
                    }

                    tran.Commit();

                }
            }
        }

        [TestMethod]
        public void excluir()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {
                    using (IBicicletaRepositorio repos = new BicicletaRepositorioEF(ctx))
                    {
                        Bicicleta entidade = new Bicicleta { Modelo = "Caloi 400 Aro 26", Preco = 987, QuantidadeEstoque = 1 };

                        repos.incluir(entidade);

                        repos.excluir(entidade.Id);
                    }

                    tran.Commit();

                }
            }
        }

        [TestMethod]
        public void obterPorId()
        {
            using (IBicicletaRepositorio repos = new BicicletaRepositorioEF())
            {
                Bicicleta entidade = repos.obterPorId(1);
            }
        }

        [TestMethod]
        public void obterTodos()
        {
            using (IBicicletaRepositorio repos = new BicicletaRepositorioEF())
            {
                List<Bicicleta> lista = repos.obterTodos();
            }
        }

        [TestMethod]
        public void obterPorModelo()
        {
            using (IBicicletaRepositorio repos = new BicicletaRepositorioEF())
            {
                List<Bicicleta> lista = repos.obterPorModelo("Teste");
            }
        }

        [TestMethod]
        public void atualizarPreco()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {
                    using (IBicicletaRepositorio repos = new BicicletaRepositorioEF(ctx))
                    {
                        Bicicleta entidade = new Bicicleta { Modelo = "Caloi 400 Aro 26", Preco = 987, QuantidadeEstoque = 1 };

                        repos.incluir(entidade);

                        repos.atualizarPreco(entidade.Id, entidade.Preco + 1);
                    }

                    tran.Commit();

                }
            }
        }

        [TestMethod]
        public void atualizarQuantidadeEstoque()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {
                    using (IBicicletaRepositorio repos = new BicicletaRepositorioEF(ctx))
                    {
                        Bicicleta entidade = new Bicicleta { Modelo = "Caloi 400 Aro 26", Preco = 987, QuantidadeEstoque = 1 };

                        repos.incluir(entidade);

                        repos.atualizarQuantidadeEstoque(entidade.Id, entidade.QuantidadeEstoque + 1);
                    }

                    tran.Commit();

                }
            }
        }


    }
}
