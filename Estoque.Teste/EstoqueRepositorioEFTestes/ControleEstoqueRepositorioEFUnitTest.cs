﻿using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Diagnostics;

namespace Estoque.Teste.EstoqueRepositorioEFTestes
{

    [TestClass]
    public class ControleEstoqueRepositorioEFUnitTest
    {
        [TestInitialize]
        public void Inicializa()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        using (ITipoMovimentacaoRepositorio repos = new TipoMovimentacaoRepositorioEF(ctx))
                        {
                            if (repos.obterPorId(1) == null)
                            {
                                repos.incluir(new TipoMovimentacao { Id = 1, Descricao = "Entrada" });
                            }
                            if (repos.obterPorId(2) == null)
                            {
                                repos.incluir(new TipoMovimentacao { Id = 2, Descricao = "Saída" });
                            }
                            if (repos.obterPorId(3) == null)
                            {
                                repos.incluir(new TipoMovimentacao { Id = 3, Descricao = "Devolução de Cliente" });
                            }
                            if (repos.obterPorId(4) == null)
                            {
                                repos.incluir(new TipoMovimentacao { Id = 4, Descricao = "Devolução a Fornecedor" });
                            }
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        EventLog.WriteEntry("Estoque.Teste", ex.Message, EventLogEntryType.Error);
                    }
                }

            }
        }

        [TestCleanup]
        public void Limpeza()
        {

        }

        [TestMethod]
        public void incluir()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {

                    Bicicleta entidadeBici;

                    using (IBicicletaRepositorio repos = new BicicletaRepositorioEF(ctx))
                    {
                        entidadeBici = new Bicicleta { Modelo = "Caloi 400 Aro 26", Preco = 987, QuantidadeEstoque = 1 };

                        repos.incluir(entidadeBici);

                    }

                    using (IControleEstoqueRepositorio repos = new ControleEstoqueRepositorioEF(ctx))
                    {
                        ControleEstoque entidadeCont = new ControleEstoque { BicicletaId = entidadeBici.Id, TipoMovimentacaoId = Convert.ToInt32(TipoMovimentacaoEnum.Entrada), Data = new DateTime(2016, 3, 20), Quantidade = 1, Valor = 987, QuantidadeAtual = 1 };

                        repos.incluir(entidadeCont);

                    }

                    tran.Commit();

                }
            }
        }


        [TestMethod]
        public void atualizar()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {

                    Bicicleta entidadeBici;

                    using (IBicicletaRepositorio repos = new BicicletaRepositorioEF(ctx))
                    {
                        entidadeBici = new Bicicleta { Modelo = "Caloi 400 Aro 26", Preco = 987, QuantidadeEstoque = 1 };

                        repos.incluir(entidadeBici);

                    }

                    using (IControleEstoqueRepositorio repos = new ControleEstoqueRepositorioEF(ctx))
                    {
                        ControleEstoque entidadeCont = new ControleEstoque { BicicletaId = entidadeBici.Id, TipoMovimentacaoId = Convert.ToInt32(TipoMovimentacaoEnum.Entrada), Data = new DateTime(2016, 3, 20), Quantidade = 1, Valor = 987, QuantidadeAtual = 1 };

                        repos.incluir(entidadeCont);

                        entidadeCont.Quantidade += 1;

                        repos.atualizar(entidadeCont);
                    }

                    tran.Commit();

                }
            }
        }

        [TestMethod]
        public void excluir()
        {
            using (EstoqueDbContext ctx = new EstoqueDbContext())
            {
                using (DbContextTransaction tran = ctx.Database.BeginTransaction())
                {

                    Bicicleta entidadeBici;

                    using (IBicicletaRepositorio repos = new BicicletaRepositorioEF(ctx))
                    {
                        entidadeBici = new Bicicleta { Modelo = "Caloi 400 Aro 26", Preco = 987, QuantidadeEstoque = 1 };

                        repos.incluir(entidadeBici);

                    }

                    using (IControleEstoqueRepositorio repos = new ControleEstoqueRepositorioEF(ctx))
                    {
                        ControleEstoque entidadeCont = new ControleEstoque { BicicletaId = entidadeBici.Id, TipoMovimentacaoId = Convert.ToInt32(TipoMovimentacaoEnum.Entrada), Data = new DateTime(2016, 3, 20), Quantidade = 1, Valor = 987, QuantidadeAtual = 1 };

                        repos.incluir(entidadeCont);

                        // Exclui
                        repos.excluir(entidadeCont.Id);
                    }

                    tran.Commit();

                }
            }
        }

        [TestMethod]
        public void obterPorId()
        {
            using (IControleEstoqueRepositorio repos = new ControleEstoqueRepositorioEF())
            {
                ControleEstoque entidade = repos.obterPorId(1);
            }
        }

        [TestMethod]
        public void obterTodos()
        {
            using (IControleEstoqueRepositorio repos = new ControleEstoqueRepositorioEF())
            {
                List<ControleEstoque> lista = repos.obterTodos();
            }
        }

        [TestMethod]
        public void obterPorBicicletaId()
        {
            using (IControleEstoqueRepositorio repos = new ControleEstoqueRepositorioEF())
            {
                int id = 1;
                List<ControleEstoque> lista = repos.obterPorBicicletaId(id);
            }
        }

        [TestMethod]
        public void obterPorPeriodo()
        {
            using (IControleEstoqueRepositorio repos = new ControleEstoqueRepositorioEF())
            {
                DateTime dataInicio = new DateTime(2016, 3, 20);
                DateTime dataFim = new DateTime(2016, 3, 20);

                List<ControleEstoque> lista = repos.obterPorPeriodo(dataInicio, dataFim);
            }
        }

        [TestMethod]
        public void obterPorBicicletaIdPeriodo()
        {
            using (IControleEstoqueRepositorio repos = new ControleEstoqueRepositorioEF())
            {
                int id = 1;
                DateTime dataInicio = new DateTime(2016, 3, 20);
                DateTime dataFim = new DateTime(2016, 3, 20);

                List<ControleEstoque> lista = repos.obterPorBicicletaIdPeriodo(id, dataInicio, dataFim);
            }
        }

    }
}
