﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace EstoqueVendasOnline
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            //CultureInfo newCulture = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            //newCulture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            //newCulture.DateTimeFormat.DateSeparator = "/";
            //Thread.CurrentThread.CurrentCulture = newCulture;

            //CultureInfo cultura = System.Globalization.CultureInfo.GetCultureInfo("pt-BR");
            //CultureInfo culturaUI = System.Globalization.CultureInfo.GetCultureInfo("pt-BR");

            //System.Globalization.CultureInfo.DefaultThreadCurrentCulture = cultura;
            //System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = culturaUI;

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
