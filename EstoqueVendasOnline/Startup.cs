﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EstoqueVendasOnline.Startup))]
namespace EstoqueVendasOnline
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
