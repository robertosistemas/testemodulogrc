﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstoqueModelo;
using EstoqueRepositorioEF;

namespace EstoqueVendasOnline.Controllers
{
    public class ControleController : Controller
    {
        private EstoqueDbContext db = new EstoqueDbContext();

        // GET: Controle
        public ActionResult Index()
        {
            var controleEstoques = db.ControleEstoques.Include(c => c.Bicicleta).Include(c => c.Tipo);
            return View(controleEstoques.ToList());
        }

        // GET: Controle/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControleEstoque controleEstoque = db.ControleEstoques.Find(id);
            if (controleEstoque == null)
            {
                return HttpNotFound();
            }
            controleEstoque.Bicicleta = db.Bicicletas.Find(controleEstoque.BicicletaId);
            controleEstoque.Tipo = db.TipoMovimentacoes.Find(controleEstoque.TipoMovimentacaoId);
            return View(controleEstoque);
        }

        // GET: Controle/Create
        public ActionResult Create()
        {
            ViewBag.BicicletaId = new SelectList(db.Bicicletas, "Id", "Modelo");
            ViewBag.TipoMovimentacaoId = new SelectList(db.TipoMovimentacoes, "Id", "Descricao");
            return View();
        }

        // POST: Controle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BicicletaId,TipoMovimentacaoId,Data,Quantidade,Valor,QuantidadeAtual")] ControleEstoque controleEstoque)
        {
            if (ModelState.IsValid)
            {
                db.ControleEstoques.Add(controleEstoque);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BicicletaId = new SelectList(db.Bicicletas, "Id", "Modelo", controleEstoque.BicicletaId);
            ViewBag.TipoMovimentacaoId = new SelectList(db.TipoMovimentacoes, "Id", "Descricao", controleEstoque.TipoMovimentacaoId);
            return View(controleEstoque);
        }

        // GET: Controle/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControleEstoque controleEstoque = db.ControleEstoques.Find(id);
            if (controleEstoque == null)
            {
                return HttpNotFound();
            }
            ViewBag.BicicletaId = new SelectList(db.Bicicletas, "Id", "Modelo", controleEstoque.BicicletaId);
            ViewBag.TipoMovimentacaoId = new SelectList(db.TipoMovimentacoes, "Id", "Descricao", controleEstoque.TipoMovimentacaoId);
            return View(controleEstoque);
        }

        // POST: Controle/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BicicletaId,TipoMovimentacaoId,Data,Quantidade,Valor,QuantidadeAtual")] ControleEstoque controleEstoque)
        {
            if (ModelState.IsValid)
            {
                db.Entry(controleEstoque).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BicicletaId = new SelectList(db.Bicicletas, "Id", "Modelo", controleEstoque.BicicletaId);
            ViewBag.TipoMovimentacaoId = new SelectList(db.TipoMovimentacoes, "Id", "Descricao", controleEstoque.TipoMovimentacaoId);
            return View(controleEstoque);
        }

        // GET: Controle/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControleEstoque controleEstoque = db.ControleEstoques.Find(id);
            if (controleEstoque == null)
            {
                return HttpNotFound();
            }
            controleEstoque.Bicicleta = db.Bicicletas.Find(controleEstoque.BicicletaId);
            controleEstoque.Tipo = db.TipoMovimentacoes.Find(controleEstoque.TipoMovimentacaoId);
            return View(controleEstoque);
        }

        // POST: Controle/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ControleEstoque controleEstoque = db.ControleEstoques.Find(id);
            db.ControleEstoques.Remove(controleEstoque);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
