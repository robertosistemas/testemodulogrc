﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EstoqueModelo;
using EstoqueRepositorioEF;

namespace EstoqueVendasOnline.Controllers
{
    public class PrecoController : Controller
    {
        private EstoqueDbContext db = new EstoqueDbContext();

        // GET: Preco
        public ActionResult Index()
        {
            var precosPropostos = db.PrecosPropostos.Include(p => p.Bicicleta).Include(p => p.GerenteEstoque).Include(p => p.GerenteLojaOnline);
            return View(precosPropostos.ToList());
        }

        // GET: Preco/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrecoProposto precoProposto = db.PrecosPropostos.Find(id);
            if (precoProposto == null)
            {
                return HttpNotFound();
            }
            precoProposto.Bicicleta = db.Bicicletas.Find(precoProposto.BicicletaId);
            precoProposto.GerenteEstoque = db.Gerentes.Find(precoProposto.GerenteEstoqueId);
            precoProposto.GerenteLojaOnline = db.Gerentes.Find(precoProposto.GerenteLojaOnlineId);
            return View(precoProposto);
        }

        // GET: Preco/Create
        public ActionResult Create()
        {
            ViewBag.BicicletaId = new SelectList(db.Bicicletas, "Id", "Modelo");
            ViewBag.GerenteEstoqueId = new SelectList(db.Gerentes, "Id", "Nome");
            ViewBag.GerenteLojaOnlineId = new SelectList(db.Gerentes, "Id", "Nome");
            PrecoProposto precoProposto = new PrecoProposto();
            return View(precoProposto);
        }

        // POST: Preco/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BicicletaId,NovoPreco,GerenteEstoqueId,GerenteLojaOnlineId,DataAlterar,HoraAlterar,ConfirmadoDataHoraAlteracao,Alterado")] PrecoProposto precoProposto)
        {
            if (ModelState.IsValid)
            {
                db.PrecosPropostos.Add(precoProposto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BicicletaId = new SelectList(db.Bicicletas, "Id", "Modelo", precoProposto.BicicletaId);
            ViewBag.GerenteEstoqueId = new SelectList(db.Gerentes, "Id", "Nome", precoProposto.GerenteEstoqueId);
            ViewBag.GerenteLojaOnlineId = new SelectList(db.Gerentes, "Id", "Nome", precoProposto.GerenteLojaOnlineId);
            return View(precoProposto);
        }

        // GET: Preco/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrecoProposto precoProposto = db.PrecosPropostos.Find(id);
            if (precoProposto == null)
            {
                return HttpNotFound();
            }
            ViewBag.BicicletaId = new SelectList(db.Bicicletas, "Id", "Modelo", precoProposto.BicicletaId);
            ViewBag.GerenteEstoqueId = new SelectList(db.Gerentes, "Id", "Nome", precoProposto.GerenteEstoqueId);
            ViewBag.GerenteLojaOnlineId = new SelectList(db.Gerentes, "Id", "Nome", precoProposto.GerenteLojaOnlineId);
            return View(precoProposto);
        }

        // POST: Preco/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BicicletaId,NovoPreco,GerenteEstoqueId,GerenteLojaOnlineId,DataAlterar,HoraAlterar,ConfirmadoDataHoraAlteracao,Alterado")] PrecoProposto precoProposto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(precoProposto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BicicletaId = new SelectList(db.Bicicletas, "Id", "Modelo", precoProposto.BicicletaId);
            ViewBag.GerenteEstoqueId = new SelectList(db.Gerentes, "Id", "Nome", precoProposto.GerenteEstoqueId);
            ViewBag.GerenteLojaOnlineId = new SelectList(db.Gerentes, "Id", "Nome", precoProposto.GerenteLojaOnlineId);
            return View(precoProposto);
        }

        // GET: Preco/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PrecoProposto precoProposto = db.PrecosPropostos.Find(id);
            if (precoProposto == null)
            {
                return HttpNotFound();
            }
            precoProposto.Bicicleta = db.Bicicletas.Find(precoProposto.BicicletaId);
            precoProposto.GerenteEstoque = db.Gerentes.Find(precoProposto.GerenteEstoqueId);
            precoProposto.GerenteLojaOnline = db.Gerentes.Find(precoProposto.GerenteLojaOnlineId);
            return View(precoProposto);
        }

        // POST: Preco/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PrecoProposto precoProposto = db.PrecosPropostos.Find(id);
            db.PrecosPropostos.Remove(precoProposto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
