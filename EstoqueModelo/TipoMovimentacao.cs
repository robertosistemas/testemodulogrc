﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace EstoqueModelo
{
    [Table("TipoMovimentacoes")]
    [DataContract(Name = "TipoMovimentacao")]
    public class TipoMovimentacao
    {
        /// <summary>
        /// Identificador do Tipo de Movimentação
        /// </summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Descrição do Tipo de Movimentação
        /// </summary>
        [Display(Name = "Tipo de Movimentação")]
        [Required]
        [StringLength(50)]
        [DataMember(Name = "Descricao")]
        public string Descricao { get; set; }

    }
}