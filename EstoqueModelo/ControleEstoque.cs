﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EstoqueModelo
{
    [KnownType(typeof(Bicicleta))]
    [KnownType(typeof(TipoMovimentacao))]
    [Table("ControleEstoques")]
    [DataContract(Name = "ControleEstoque")]
    public class ControleEstoque
    {
        /// <summary>
        /// Identificador da Movimentação
        /// </summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Identificação da bicicleta (Produto)
        /// </summary>
        [Display(Name = "Modelo")]
        [DataMember(Name = "BicicletaId")]
        public int BicicletaId { get; set; }

        [Display(Name = "Modelo")]
        [ForeignKey("BicicletaId")]
        [IgnoreDataMember, XmlIgnore]
        public virtual Bicicleta Bicicleta { get; set; }

        /// <summary>
        /// Identificação de do tipo de movimentação
        /// </summary>
        [Display(Name = "Tipo de Movimentação")]
        [Required]
        [Range(1, 4)]
        [DataMember(Name = "TipoMovimentacaoId")]
        public int TipoMovimentacaoId { get; set; }

        [Display(Name = "Tipo de Movimentação")]
        [ForeignKey("TipoMovimentacaoId")]
        [IgnoreDataMember, XmlIgnore]
        public virtual TipoMovimentacao Tipo { get; set; }

        /// <summary>
        /// Data da Movimentação
        /// </summary>
        [Display(Name = "Data da Movimentação")]
        [Column(TypeName = "datetime2")]
        [DataType(DataType.Date, ErrorMessage = "É necessário informar uma data no seguinte formato: dd/mm/aaaa")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = false)]
        [DataMember(Name = "Data")]
        public DateTime Data { get; set; }

        /// <summary>
        /// Quantidade de produto movimentado
        /// </summary>
        [Display(Name = "Quantidade")]
        [Required]
        [DataMember(Name = "Quantidade")]
        public int Quantidade { get; set; }

        /// <summary>
        /// Valor movimentando
        /// </summary>
        [Display(Name = "Valor")]
        [Required]
        [DataType(DataType.Currency)]
        [DataMember(Name = "Valor")]
        public decimal Valor { get; set; }

        /// <summary>
        /// Quantidade atual em estoque
        /// </summary>
        [Display(Name = "Quantidade Atual")]
        [DataMember(Name = "QuantidadeAtual")]
        public int QuantidadeAtual { get; set; }

    }
}