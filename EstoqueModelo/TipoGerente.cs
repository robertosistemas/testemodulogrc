﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace EstoqueModelo
{
    [Table("TipoGerentes")]
    [DataContract(Name = "TipoGerente")]
    public class TipoGerente
    {

        /// <summary>
        /// Identificador do Gerente
        /// </summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Descrição do Tipo de Gerente
        /// </summary>
        [Display(Name = "Tipo de Gerente")]
        [Required]
        [StringLength(50)]
        [DataMember(Name = "Descricao")]
        public string Descricao { get; set; }

    }
}