﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace EstoqueModelo
{
    [Table("Bicicletas")]
    [DataContract(Name = "Bicicleta")]
    public class Bicicleta
    {
        /// <summary>
        /// Identificador da bicicleta
        /// </summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Descrição do modelo da bicicleta
        /// </summary>
        [Display(Name = "Modelo")]
        [Required]
        [StringLength(50)]
        [DataMember(Name = "Modelo")]
        public string Modelo { get; set; }

        /// <summary>
        /// Preço de Venda
        /// </summary>
        [Display(Name = "Preço de Venda")]
        [DataType(DataType.Currency)]
        [DataMember(Name = "Preco")]
        public decimal Preco { get; set; }

        /// <summary>
        /// Quantidade em estoque
        /// </summary>
        [Display(Name = "Quantidade em Estoque")]
        [DataMember(Name = "QuantidadeEstoque")]
        public int QuantidadeEstoque { get; set; }

        ///// <summary>
        ///// Referência ao registro de estoque para uma determinada bicicleta
        ///// </summary>
        //[IgnoreDataMember, XmlIgnore]
        //public virtual ICollection<ControleEstoque> ControleEstoques { get; set; }

    }
}