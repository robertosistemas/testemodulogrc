﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace EstoqueModelo
{
    [Table("Dominios")]
    [DataContract(Name = "Dominios")]
    public class Dominio
    {
        /// <summary>
        /// Identificador do domínio
        /// </summary>
        [Display(Name = "Id")]
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Tipo de Domínio
        /// </summary>
        [Display(Name = "Tipo")]
        [Required]
        [DataMember(Name = "Tipo")]
        public int Tipo { get; set; }

        /// <summary>
        /// Valor do domínio
        /// </summary>
        [Display(Name = "Valor")]
        [Required]
        [DataMember(Name = "Valor")]
        public int Valor { get; set; }

        /// <summary>
        /// Descrição do domínio
        /// </summary>
        [Display(Name = "Descrição")]
        [Required]
        [StringLength(50)]
        [DataMember(Name = "Descricao")]
        public string Descricao { get; set; }

    }
}