﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EstoqueModelo
{
    [KnownType(typeof(Bicicleta))]
    [KnownType(typeof(Gerente))]
    [Table("PrecosPropostos")]
    [DataContract(Name = "PrecoProposto")]
    public class PrecoProposto
    {
        /// <summary>
        /// Identificador do Preço proposto
        /// </summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// Identificação da bicicleta (Produto)
        /// </summary>
        [Display(Name = "Id da Bicicleta")]
        [Required]
        [DataMember(Name = "BicicletaId")]
        public int BicicletaId { get; set; }

        [ForeignKey("BicicletaId")]
        [IgnoreDataMember, XmlIgnore]
        public virtual Bicicleta Bicicleta { get; set; }

        /// <summary>
        /// Novo Preço proposto
        /// </summary>
        //[Required(ErrorMessage = "É necessário digitar um valor para o campo novo preço")]
        //[Range(typeof(decimal), "0,01", "100000,00", ErrorMessage = "Entre com um valor decimal válido")]
        //[RegularExpression(@"^\[0-9]{1,6}\.[0-9]{2}$", ErrorMessage = "Entre com um valor decimal no formato 999,99")]
        //[HtmlProperties(CssClass = "currency")]
        [Display(Name = "Novo preço")]
        [UIHint("Currency")]
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Currency, ErrorMessage = "É necessário informar uma valor no seguinte formato: R$ 999.999,99")]
        [DataMember(Name = "NovoPreco")]
        public decimal NovoPreco { get; set; }

        /// <summary>
        /// Referência ao gerente de Estoque
        /// </summary>
        [Display(Name = "Gerente estoque")]
        [Required]
        [DataMember(Name = "GerenteEstoqueId")]
        public int GerenteEstoqueId { get; set; }

        [Display(Name = "Gerente estoque")]
        [ForeignKey("GerenteEstoqueId")]
        [IgnoreDataMember, XmlIgnore]
        public virtual Gerente GerenteEstoque { get; set; }

        /// <summary>
        /// Referência ao gerente da Loja online
        /// </summary>
        [Display(Name = "Gerente loja online")]
        [Required]
        [DataMember(Name = "GerenteLojaOnlineId")]
        public int GerenteLojaOnlineId { get; set; }

        [Display(Name = "Gerente loja online")]
        [ForeignKey("GerenteLojaOnlineId")]
        [IgnoreDataMember, XmlIgnore]
        public virtual Gerente GerenteLojaOnline { get; set; }

        /// <summary>
        /// Data que o gerente da loja online agendou a alteração do preço
        /// </summary>
        [UIHint("Date")]
        [Display(Name = "Data para alteração")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date, ErrorMessage = "É necessário informar uma data no seguinte formato: dd/mm/aaaa")]
        [Column(TypeName = "datetime2")]
        [DataMember(Name = "DataAlterar")]
        public Nullable<DateTime> DataAlterar { get; set; }

        /// <summary>
        /// Data que o gerente da loja online agendou a alteração do preço
        /// </summary>
        [Display(Name = "Hora para alteração")]
        [DisplayFormat(DataFormatString = "{0:t}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Time, ErrorMessage = "É necessário informar uma hora no seguinte formato: hh:mm")]
        [Column(TypeName = "time")]
        [DataMember(Name = "HoraAlterar")]
        public Nullable<TimeSpan> HoraAlterar { get; set; }

        /// <summary>
        /// Indica se a data e horário para alteração foi confirmado pelo gerente de vendas online
        /// </summary>
        [Display(Name = "Confirmado data hora")]
        [Required]
        [DataMember(Name = "ConfirmadoDataHoraAlteracao")]
        public bool ConfirmadoDataHoraAlteracao { get; set; }

        /// <summary>
        /// Indica se já foi alterado
        /// </summary>
        [Display(Name = "Já foi alterado")]
        [Required]
        [DataMember(Name = "Alterado")]
        public bool Alterado { get; set; }
    }
}