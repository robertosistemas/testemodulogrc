﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EstoqueModelo
{
    [KnownType(typeof(TipoGerente))]
    [Table("Gerentes")]
    [DataContract(Name = "Gerente")]
    public class Gerente
    {
        /// <summary>
        /// Identificador do Gerente
        /// </summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Identificação do tipo de gerente
        /// </summary>
        [Display(Name = "Tipo de Gerente")]
        [Required]
        [Range(1, 2)]
        [DataMember(Name = "TipoGerenteId")]
        public int TipoGerenteId { get; set; }

        [Display(Name = "Tipo de Gerente")]
        [ForeignKey("TipoGerenteId")]
        [IgnoreDataMember, XmlIgnore]
        public virtual TipoGerente Tipo { get; set; }

        /// <summary>
        /// Nome do Gerente
        /// </summary>
        [Display(Name = "Nome Gerente")]
        [Required]
        [StringLength(50)]
        [DataMember(Name = "Nome")]
        public string Nome { get; set; }

    }
}