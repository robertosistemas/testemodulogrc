﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace EstoqueModelo
{
    public static class Enumeracoes
    {
        /// <summary>
        /// Retorna uma lista de domínios para o enumerador informado
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public static List<Dominio> ObterTodosEnum<TEnum>() where TEnum : struct
        {
            var enumerationType = typeof(TEnum);

            if (!enumerationType.IsEnum)
                throw new ArgumentException("É esperado um tipo enumeração.");

            var lista = new List<Dominio>();

            foreach (int valor in Enum.GetValues(enumerationType))
            {
                var item = Enum.Parse(enumerationType, valor.ToString());
                var descricaoAtributo = Attribute.GetCustomAttribute(item.GetType().GetField(item.ToString()), typeof(DescriptionAttribute)) as DescriptionAttribute;
                string descricao;
                if (descricaoAtributo != null)
                {
                    descricao = descricaoAtributo.Description;
                }
                else
                {
                    descricao = Enum.GetName(enumerationType, valor);
                }
                lista.Add(new Dominio { Valor = valor, Descricao = descricao });
            }

            return lista.OrderBy(item => item.Descricao).ToList<Dominio>();
        }

        /// <summary>
        /// Retorna um item de um enumerador expecificado através do seu valor ou descrição
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public static object obterEnumercao<TEnum>(string item) where TEnum : struct
        {
            var enumerationType = typeof(TEnum);
            if (!enumerationType.IsEnum)
                throw new ArgumentException("É esperado um tipo enumeração.");
            return (TEnum)Enum.Parse(enumerationType, item);
        }

        /// <summary>
        /// Retorna uma lista com todos os itens do enumeradore TipoMovimentacao
        /// </summary>
        /// <returns></returns>
        public static List<Dominio> obterTodosTipoMovimentacao()
        {
            return Enumeracoes.ObterTodosEnum<TipoMovimentacaoEnum>();
        }

        /// <summary>
        /// Retorna uma lista com todos os itens do enumeradore TipoGerente
        /// </summary>
        /// <returns></returns>
        public static List<Dominio> obterTodosTipoGerente()
        {
            return Enumeracoes.ObterTodosEnum<TipoGerenteEnum>();
        }
    }

    /// <summary>
    /// Tipo de Gerentes
    /// </summary>
    public enum TipoGerenteEnum
    {
        [Description("(Selecione)")]
        Nenhum = -1,
        [Description("Gerente de Estoque")]
        Estoque = 1,
        [Description("Gerente de Vendas online")]
        Vendas = 2
    }

    /// <summary>
    /// Tipos de Movimentação de Estoque
    /// </summary>
    public enum TipoMovimentacaoEnum
    {
        [Description("(Selecione)")]
        Nenhum = -1,
        [Description("Entrada")]
        Entrada = 1, // Utilizado para sinalizar compra de produto (Entrada)
        [Description("Saída")]
        Saida = 2, // Utilizado para Venda de produtos (Saída)
        [Description("Devolução de Cliente")]
        DevolucaoCliente = 3, // Utilizado para devolução de Clientes (Entrada)
        [Description("Devolução a Fornecedor")]
        DevolucaoFonecedor = 4, // Utilizado para devolução a fornecedor (Saída)
    }

    /// <summary>
    /// Classifica os tipo de Enumeradores na aplicação
    /// </summary>
    public enum TipoDominioEnum
    {
        [Description("(Selecione)")]
        Nenhum = -1,
        [Description("Tipo de Gerentes")]
        TipoGerente = 1,
        [Description("Tipos de Movimentação de Estoque")]
        TipoMovimentacao = 2
    }
}