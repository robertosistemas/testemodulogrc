﻿using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using System.Data.SqlClient;
using System.Text;

namespace EstoqueWinService
{
    public partial class AtualizaPrecos : ServiceBase
    {
        public AtualizaPrecos()
        {
            InitializeComponent();
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("pt-BR");
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("pt-BR");
        }

        private double Intervalo = 10000; // 10 segundos por padrão
        private Timer Agendamento;

        protected override void OnStart(string[] args)
        {
            try
            {
                Intervalo = Convert.ToDouble(ConfigurationManager.AppSettings.Get("Intervalo").ToString());
                Agendamento = new Timer(Intervalo);
                Agendamento.Elapsed += OnTimedEvent;
                Agendamento.AutoReset = true;
                Agendamento.Enabled = true;
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("AtualizaPrecos", e.Message, EventLogEntryType.Error);
            }
        }

        protected override void OnStop()
        {
            try
            {
                Agendamento.Enabled = false;
                Agendamento.Dispose();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("AtualizaPrecos", e.Message, EventLogEntryType.Error);
            }
        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            Agendamento.Enabled = false;
            try
            {
                AtualizaPrecosAction();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AtualizaPrecos", ex.Message, EventLogEntryType.Error);
            }
            finally
            {
                Agendamento.Enabled = true;
            }
        }

        public void AtualizaPrecosAction()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["EstoqueDbContext"].ConnectionString))
                {

                    conn.Open();

                    using (SqlTransaction tran = conn.BeginTransaction())
                    {
                        try
                        {
                            StringBuilder sql = new StringBuilder();
                            sql.AppendLine(" SELECT ");
                            sql.AppendLine("      [Id] ");
                            sql.AppendLine("     ,[BicicletaId] ");
                            sql.AppendLine("     ,[NovoPreco] ");
                            sql.AppendLine(" FROM [dbo].[PrecosPropostos] ");
                            sql.AppendLine(" WHERE ");
                            sql.AppendLine("     ((([DataAlterar] < @DataAlterarA) OR (([DataAlterar] = @DataAlterarB) AND ([HoraAlterar] <= @HoraAlterar))) AND ([ConfirmadoDataHoraAlteracao] = @ConfirmadoDataHoraAlteracao) AND ([Alterado] = @Alterado)); ");

                            List<PrecoProposto> Itens;

                            using (SqlCommand cmd = new SqlCommand(sql.ToString(), conn, tran))
                            {
                                int dia = DateTime.Now.Day;
                                int mes = DateTime.Now.Month;
                                int ano = DateTime.Now.Year;
                                int horas = DateTime.Now.Hour;
                                int minutos = DateTime.Now.Minute;
                                int segundos = DateTime.Now.Second;
                                cmd.Parameters.Add(new SqlParameter("DataAlterarA", new DateTime(ano, mes, dia)));
                                cmd.Parameters.Add(new SqlParameter("DataAlterarB", new DateTime(ano, mes, dia)));
                                cmd.Parameters.Add(new SqlParameter("HoraAlterar", new TimeSpan(horas, minutos, segundos)));
                                cmd.Parameters.Add(new SqlParameter("ConfirmadoDataHoraAlteracao", true));
                                cmd.Parameters.Add(new SqlParameter("Alterado", false));
                                using (SqlDataReader dr = cmd.ExecuteReader())
                                {
                                    Itens = DataReaderToListaPreco(dr);
                                }
                            }

                            foreach (PrecoProposto Item in Itens)
                            {
                                StringBuilder sqlUpd = new StringBuilder();
                                sqlUpd.AppendLine(" UPDATE [dbo].[Bicicletas] ");
                                sqlUpd.AppendLine("     SET [Preco] = @Preco ");
                                sqlUpd.AppendLine(" WHERE ");
                                sqlUpd.AppendLine("     [Id] = @Id; ");

                                using (SqlCommand cmdUpd = new SqlCommand(sqlUpd.ToString(), conn, tran))
                                {
                                    cmdUpd.Parameters.Add(new SqlParameter("Preco", Item.NovoPreco));
                                    cmdUpd.Parameters.Add(new SqlParameter("Id", Item.BicicletaId));
                                    cmdUpd.ExecuteNonQuery();
                                }

                                sqlUpd = new StringBuilder();
                                sqlUpd.AppendLine(" UPDATE [dbo].[PrecosPropostos] ");
                                sqlUpd.AppendLine("     SET [Alterado] = @Alterado ");
                                sqlUpd.AppendLine(" WHERE ");
                                sqlUpd.AppendLine("     [Id] = @Id; ");

                                using (SqlCommand cmdUpd = new SqlCommand(sqlUpd.ToString(), conn, tran))
                                {
                                    cmdUpd.Parameters.Add(new SqlParameter("Alterado", true));
                                    cmdUpd.Parameters.Add(new SqlParameter("Id", Item.Id));
                                    cmdUpd.ExecuteNonQuery();
                                }
                            }

                            tran.Commit();
                        }

                        catch (Exception ex)
                        {
                            tran.Rollback();
                            EventLog.WriteEntry("AtualizaPrecos", ex.Message, EventLogEntryType.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("AtualizaPrecos", ex.Message, EventLogEntryType.Error);
            }
        }

        List<PrecoProposto> DataReaderToListaPreco(SqlDataReader dr)
        {
            List<PrecoProposto> lista = new List<PrecoProposto>();
            while (dr.Read())
            {
                lista.Add(new PrecoProposto { Id = dr.GetInt32(dr.GetOrdinal("Id")), BicicletaId = dr.GetInt32(dr.GetOrdinal("BicicletaId")), NovoPreco = dr.GetDecimal(dr.GetOrdinal("NovoPreco")), });
            }
            return lista;
        }

    }
}
