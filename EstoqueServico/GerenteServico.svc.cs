﻿using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;
using System.Collections.Generic;

namespace EstoqueServico
{
    public class GerenteServico : IGerenteServico
    {
        public void incluir(Gerente entidade)
        {
            using (IGerenteRepositorio repo = new GerenteRepositorioEF())
            {
                repo.incluir(entidade);
            }
        }

        public void atualizar(Gerente entidade)
        {
            using (IGerenteRepositorio repo = new GerenteRepositorioEF())
            {
                repo.atualizar(entidade);
            }
        }

        public void excluir(int id)
        {
            using (IGerenteRepositorio repo = new GerenteRepositorioEF())
            {
                repo.excluir(id);
            }
        }

        public Gerente obterPorId(int id)
        {
            Gerente gere;
            using (IGerenteRepositorio repo = new GerenteRepositorioEF())
            {
                gere = repo.obterPorId(id);
            }
            return gere;
        }

        public List<Gerente> obterTodos()
        {
            List<Gerente> geres;
            using (IGerenteRepositorio repo = new GerenteRepositorioEF())
            {
                geres = repo.obterTodos();
            }
            System.Threading.Thread.Sleep(500);
            return geres;
        }

        public List<Gerente> obterPorTipoGerenteId(int tipoGerenteId)
        {
            List<Gerente> geres;
            using (IGerenteRepositorio repo = new GerenteRepositorioEF())
            {
                geres = repo.obterPorTipoGerenteId(tipoGerenteId);
            }
            return geres;
        }

        public List<Gerente> obterPorNome(string nome)
        {
            List<Gerente> geres;
            using (IGerenteRepositorio repo = new GerenteRepositorioEF())
            {
                geres = repo.obterPorNome(nome);
            }
            return geres;
        }
    }
}