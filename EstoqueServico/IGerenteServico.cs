﻿using EstoqueModelo;
using System.Collections.Generic;
using System.ServiceModel;

namespace EstoqueServico
{
    [ServiceContract]
    public interface IGerenteServico
    {
        [OperationContract]
        void incluir(Gerente entidade);

        [OperationContract]
        void atualizar(Gerente entidade);

        [OperationContract]
        void excluir(int id);

        [OperationContract]
        Gerente obterPorId(int id);

        [OperationContract]
        List<Gerente> obterTodos();

        [OperationContract]
        List<Gerente> obterPorTipoGerenteId(int tipoGerenteId);

        [OperationContract]
        List<Gerente> obterPorNome(string nome);
    }
}