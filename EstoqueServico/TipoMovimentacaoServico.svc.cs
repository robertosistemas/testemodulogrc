﻿using System;
using System.Collections.Generic;
using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;

namespace EstoqueServico
{
    public class TipoMovimentacaoServico : ITipoMovimentacaoServico
    {
        public void incluir(TipoMovimentacao entidade)
        {
            using (ITipoMovimentacaoRepositorio repo = new TipoMovimentacaoRepositorioEF())
            {
                repo.incluir(entidade);
            }
        }

        public void atualizar(TipoMovimentacao entidade)
        {
            using (ITipoMovimentacaoRepositorio repo = new TipoMovimentacaoRepositorioEF())
            {
                repo.atualizar(entidade);
            }
        }

        public void excluir(int id)
        {
            using (ITipoMovimentacaoRepositorio repo = new TipoMovimentacaoRepositorioEF())
            {
                repo.excluir(id);
            }
        }

        public TipoMovimentacao obterPorId(int id)
        {
            TipoMovimentacao tipo;
            using (ITipoMovimentacaoRepositorio repo = new TipoMovimentacaoRepositorioEF())
            {
                tipo = repo.obterPorId(id);
            }
            return tipo;
        }

        public List<TipoMovimentacao> obterTodos()
        {
            List<TipoMovimentacao> tipos;
            using (ITipoMovimentacaoRepositorio repo = new TipoMovimentacaoRepositorioEF())
            {
                tipos = repo.obterTodos();
            }
            return tipos;
        }
    }
}