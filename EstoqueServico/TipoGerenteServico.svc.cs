﻿using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;
using System.Collections.Generic;

namespace EstoqueServico
{
    public class TipoGerenteServico : ITipoGerenteServico
    {
        public void incluir(TipoGerente entidade)
        {
            using (ITipoGerenteRepositorio repo = new TipoGerenteRepositorioEF())
            {
                repo.incluir(entidade);
            }
        }

        public void atualizar(TipoGerente entidade)
        {
            using (ITipoGerenteRepositorio repo = new TipoGerenteRepositorioEF())
            {
                repo.atualizar(entidade);
            }
        }

        public void excluir(int id)
        {
            using (ITipoGerenteRepositorio repo = new TipoGerenteRepositorioEF())
            {
                repo.excluir(id);
            }
        }

        public TipoGerente obterPorId(int id)
        {
            TipoGerente tipo;
            using (ITipoGerenteRepositorio repo = new TipoGerenteRepositorioEF())
            {
                tipo = repo.obterPorId(id);
            }
            return tipo;
        }

        public List<TipoGerente> obterTodos()
        {
            List<TipoGerente> tipos;
            using (ITipoGerenteRepositorio repo = new TipoGerenteRepositorioEF())
            {
                tipos = repo.obterTodos();
            }
            return tipos;
        }
    }
}