﻿using EstoqueModelo;
using System.Collections.Generic;
using System.ServiceModel;

namespace EstoqueServico
{
    [ServiceContract]
    public interface ITipoMovimentacaoServico
    {
        [OperationContract]
        void incluir(TipoMovimentacao entidade);

        [OperationContract]
        void atualizar(TipoMovimentacao entidade);

        [OperationContract]
        void excluir(int id);

        [OperationContract]
        TipoMovimentacao obterPorId(int id);

        [OperationContract]
        List<TipoMovimentacao> obterTodos();
    }
}