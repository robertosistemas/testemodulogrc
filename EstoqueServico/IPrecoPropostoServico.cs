﻿using EstoqueModelo;
using System.Collections.Generic;
using System.ServiceModel;

namespace EstoqueServico
{
    [ServiceContract]
    public interface IPrecoPropostoServico
    {
        [OperationContract]
        void incluir(PrecoProposto entidade);

        [OperationContract]
        void atualizar(PrecoProposto entidade);

        [OperationContract]
        void excluir(int id);

        [OperationContract]
        PrecoProposto obterPorId(int id);

        [OperationContract]
        List<PrecoProposto> obterTodos();

        [OperationContract]
        List<PrecoProposto> obterPorBicicletaId(int bicicletaId);

        [OperationContract]
        List<PrecoProposto> obterPorGerenteEstoqueId(int gerenteEstoqueId);

        [OperationContract]
        List<PrecoProposto> obterPorGerenteLojaOnlineId(int gerenteLojaOnlineId);

        [OperationContract]
        List<PrecoProposto> obterRegistroAgendar();

        [OperationContract]
        List<PrecoProposto> obterRegistroProcessar();
    }
}