﻿using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;
using System.Collections.Generic;

namespace EstoqueServico
{
    public class PrecoPropostoServico : IPrecoPropostoServico
    {
        public void incluir(PrecoProposto entidade)
        {
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                repo.incluir(entidade);
            }
        }

        public void atualizar(PrecoProposto entidade)
        {
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                repo.atualizar(entidade);
            }
        }

        public void excluir(int id)
        {
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                repo.excluir(id);
            }
        }

        public PrecoProposto obterPorId(int id)
        {
            PrecoProposto prec;
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                prec = repo.obterPorId(id);
            }
            return prec;
        }

        public List<PrecoProposto> obterTodos()
        {
            List<PrecoProposto> precs;
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                precs = repo.obterTodos();
            }
            System.Threading.Thread.Sleep(500);
            return precs;
        }

        public List<PrecoProposto> obterPorBicicletaId(int bicicletaId)
        {
            List<PrecoProposto> precs;
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                precs = repo.obterPorBicicletaId(bicicletaId);
            }
            return precs;
        }

        public List<PrecoProposto> obterPorGerenteEstoqueId(int gerenteEstoqueId)
        {
            List<PrecoProposto> precs;
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                precs = repo.obterPorGerenteEstoqueId(gerenteEstoqueId);
            }
            return precs;
        }

        public List<PrecoProposto> obterPorGerenteLojaOnlineId(int gerenteLojaOnlineId)
        {
            List<PrecoProposto> precs;
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                precs = repo.obterPorGerenteLojaOnlineId(gerenteLojaOnlineId);
            }
            return precs;
        }

        public List<PrecoProposto> obterRegistroAgendar()
        {
            List<PrecoProposto> precs;
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                precs = repo.obterRegistroAgendar();
            }
            return precs;
        }

        public List<PrecoProposto> obterRegistroProcessar()
        {
            List<PrecoProposto> precs;
            using (IPrecoPropostoRepositorio repo = new PrecoPropostoRepositorioEF())
            {
                precs = repo.obterRegistroProcessar();
            }
            return precs;
        }
    }
}