﻿using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;
using System.Collections.Generic;

namespace EstoqueServico
{
    public class BicicletaServico : IBicicletaServico
    {
        public void incluir(Bicicleta entidade)
        {
            using (IBicicletaRepositorio repo = new BicicletaRepositorioEF())
            {
                repo.incluir(entidade);
            }
        }

        public void atualizar(Bicicleta entidade)
        {
            using (IBicicletaRepositorio repo = new BicicletaRepositorioEF())
            {
                repo.atualizar(entidade);
            }
        }

        public void excluir(int id)
        {
            using (IBicicletaRepositorio repo = new BicicletaRepositorioEF())
            {
                repo.excluir(id);
            }
        }

        public Bicicleta obterPorId(int id)
        {
            Bicicleta bici;
            using (IBicicletaRepositorio repo = new BicicletaRepositorioEF())
            {
                bici = repo.obterPorId(id);
            }
            return bici;
        }

        public List<Bicicleta> obterTodos()
        {
            List<Bicicleta> bicis;
            using (IBicicletaRepositorio repo = new BicicletaRepositorioEF())
            {
                bicis = repo.obterTodos();
            }
            System.Threading.Thread.Sleep(500);
            return bicis;
        }

        public List<Bicicleta> obterPorModelo(string modelo)
        {
            List<Bicicleta> bicis;
            using (IBicicletaRepositorio repo = new BicicletaRepositorioEF())
            {
                bicis = repo.obterPorModelo(modelo);
            }
            return bicis;
        }

        public void atualizarPreco(int id, decimal novoPreco)
        {
            using (IBicicletaRepositorio repo = new BicicletaRepositorioEF())
            {
                repo.atualizarPreco(id, novoPreco);
            }
        }

        public void atualizarQuantidadeEstoque(int id, int novaQuantidade)
        {
            using (IBicicletaRepositorio repo = new BicicletaRepositorioEF())
            {
                repo.atualizarQuantidadeEstoque(id, novaQuantidade);
            }
        }
    }
}