﻿using EstoqueModelo;
using EstoqueRepositorio;
using EstoqueRepositorioEF;
using System;
using System.Collections.Generic;

namespace EstoqueServico
{
    public class ControleEstoqueServico : IControleEstoqueServico
    {

        public void incluir(ControleEstoque entidade)
        {
            using (IControleEstoqueRepositorio repo = new ControleEstoqueRepositorioEF())
            {
                repo.incluir(entidade);
            }
        }

        public void atualizar(ControleEstoque entidade)
        {
            using (IControleEstoqueRepositorio repo = new ControleEstoqueRepositorioEF())
            {
                repo.atualizar(entidade);
            }
        }

        public void excluir(int id)
        {
            using (IControleEstoqueRepositorio repo = new ControleEstoqueRepositorioEF())
            {
                repo.excluir(id);
            }
        }

        public ControleEstoque obterPorId(int id)
        {
            ControleEstoque cont;
            using (IControleEstoqueRepositorio repo = new ControleEstoqueRepositorioEF())
            {
                cont = repo.obterPorId(id);
            }
            return cont;
        }

        public List<ControleEstoque> obterTodos()
        {
            List<ControleEstoque> conts;
            using (IControleEstoqueRepositorio repo = new ControleEstoqueRepositorioEF())
            {
                conts = repo.obterTodos();
            }
            System.Threading.Thread.Sleep(500);
            return conts;
        }

        public List<ControleEstoque> obterPorBicicletaId(int bicicletaId)
        {
            List<ControleEstoque> conts;
            using (IControleEstoqueRepositorio repo = new ControleEstoqueRepositorioEF())
            {
                conts = repo.obterPorBicicletaId(bicicletaId);
            }
            return conts;
        }

        public List<ControleEstoque> obterPorPeriodo(DateTime dataInicio, DateTime dataFim)
        {
            List<ControleEstoque> conts;
            using (IControleEstoqueRepositorio repo = new ControleEstoqueRepositorioEF())
            {
                conts = repo.obterPorPeriodo(dataInicio, dataFim);
            }
            return conts;
        }

        public List<ControleEstoque> obterPorBicicletaIdPeriodo(int bicicletaId, DateTime dataInicio, DateTime dataFim)
        {
            List<ControleEstoque> conts;
            using (IControleEstoqueRepositorio repo = new ControleEstoqueRepositorioEF())
            {
                conts = repo.obterPorBicicletaIdPeriodo(bicicletaId, dataInicio, dataFim);
            }
            return conts;
        }
    }
}