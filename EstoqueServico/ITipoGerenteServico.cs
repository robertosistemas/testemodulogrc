﻿using EstoqueModelo;
using System.Collections.Generic;
using System.ServiceModel;

namespace EstoqueServico
{
    [ServiceContract]
    public interface ITipoGerenteServico
    {
        [OperationContract]
        void incluir(TipoGerente entidade);

        [OperationContract]
        void atualizar(TipoGerente entidade);

        [OperationContract]
        void excluir(int id);

        [OperationContract]
        TipoGerente obterPorId(int id);

        [OperationContract]
        List<TipoGerente> obterTodos();
    }
}