﻿using EstoqueModelo;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace EstoqueServico
{
    [ServiceContract]
    public interface IControleEstoqueServico
    {
        [OperationContract]
        void incluir(ControleEstoque entidade);

        [OperationContract]
        void atualizar(ControleEstoque entidade);

        [OperationContract]
        void excluir(int id);

        [OperationContract]
        ControleEstoque obterPorId(int id);

        [OperationContract]
        List<ControleEstoque> obterTodos();

        [OperationContract]
        List<ControleEstoque> obterPorBicicletaId(int bicicletaId);

        [OperationContract]
        List<ControleEstoque> obterPorPeriodo(DateTime dataInicio, DateTime dataFim);

        [OperationContract]
        List<ControleEstoque> obterPorBicicletaIdPeriodo(int bicicletaId, DateTime dataInicio, DateTime dataFim);
    }
}