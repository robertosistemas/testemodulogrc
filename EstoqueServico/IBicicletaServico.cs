﻿using EstoqueModelo;
using System.Collections.Generic;
using System.ServiceModel;

namespace EstoqueServico
{
    [ServiceContract]
    interface IBicicletaServico
    {
        [OperationContract]
        void incluir(Bicicleta entidade);

        [OperationContract]
        void atualizar(Bicicleta entidade);

        [OperationContract]
        void excluir(int id);

        [OperationContract]
        Bicicleta obterPorId(int id);

        [OperationContract]
        List<Bicicleta> obterTodos();

        [OperationContract]
        List<Bicicleta> obterPorModelo(string modelo);

        [OperationContract]
        void atualizarPreco(int id, decimal novoPreco);

        [OperationContract]
        void atualizarQuantidadeEstoque(int id, int novaQuantidade);
    }
}