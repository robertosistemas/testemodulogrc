namespace EstoqueRepositorioEF.Migrations
{
    using EstoqueModelo;
    using System;
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<EstoqueDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(EstoqueDbContext context)
        {

            // Dom�nios

            // Tipo de Gerente
            //context.Dominios.AddOrUpdate(new Dominio { Id = 1, Tipo = 1, Valor = -1, Descricao = "(Selecione)" });
            context.Dominios.AddOrUpdate(new Dominio { Id = 2, Tipo = 1, Valor = 1, Descricao = "Gerente de Estoque" });
            context.Dominios.AddOrUpdate(new Dominio { Id = 3, Tipo = 1, Valor = 2, Descricao = "Gerente de Vendas online" });

            // Tipo de Movimenta��o
            //context.Dominios.AddOrUpdate(new Dominio { Id = 4, Tipo = 2, Valor = -1, Descricao = "(Selecione)" });
            context.Dominios.AddOrUpdate(new Dominio { Id = 5, Tipo = 2, Valor = 1, Descricao = "Entrada" });
            context.Dominios.AddOrUpdate(new Dominio { Id = 6, Tipo = 2, Valor = 2, Descricao = "Sa�da" });
            context.Dominios.AddOrUpdate(new Dominio { Id = 7, Tipo = 2, Valor = 3, Descricao = "Devolu��o de Cliente" });
            context.Dominios.AddOrUpdate(new Dominio { Id = 8, Tipo = 2, Valor = 4, Descricao = "Devolu��o a Fornecedor" });

            // Tipo de Gerentes
            context.TipoGerentes.AddOrUpdate(new TipoGerente { Id = 1, Descricao = "Gerente de Estoque" });
            context.TipoGerentes.AddOrUpdate(new TipoGerente { Id = 2, Descricao = "Gerente de Vendas online" });

            // Adiciona Gerentes
            context.Gerentes.AddOrUpdate(new Gerente { Id = 1, TipoGerenteId = Convert.ToInt32(TipoGerenteEnum.Estoque), Nome = "Roberto Carlos" });
            context.Gerentes.AddOrUpdate(new Gerente { Id = 2, TipoGerenteId = Convert.ToInt32(TipoGerenteEnum.Vendas), Nome = "Nivia Vieira" });

            // Adiciona Bicicletas
            context.Bicicletas.AddOrUpdate(new Bicicleta { Id = 1, Modelo = "Caloi 400 Aro 26", Preco = -1, QuantidadeEstoque = 1 });
            context.Bicicletas.AddOrUpdate(new Bicicleta { Id = 2, Modelo = "Verden Bikes Breeze Aro 20", Preco = -2, QuantidadeEstoque = 1 });

            // Pre�o Proposto
            context.PrecosPropostos.AddOrUpdate(new PrecoProposto { Id = 1, BicicletaId = 1, NovoPreco = 987, GerenteEstoqueId = 1, GerenteLojaOnlineId = 2, DataAlterar = new DateTime(2016, 3, 20), HoraAlterar = new TimeSpan(12, 0, 0), ConfirmadoDataHoraAlteracao = false, Alterado = false });
            context.PrecosPropostos.AddOrUpdate(new PrecoProposto { Id = 2, BicicletaId = 2, NovoPreco = 750, GerenteEstoqueId = 1, GerenteLojaOnlineId = 2, DataAlterar = new DateTime(2016, 3, 20), HoraAlterar = new TimeSpan(12, 0, 0), ConfirmadoDataHoraAlteracao = false, Alterado = false });

            // Tipo de Movimenta��o
            context.TipoMovimentacoes.AddOrUpdate(new TipoMovimentacao { Id = 1, Descricao = "Entrada" });
            context.TipoMovimentacoes.AddOrUpdate(new TipoMovimentacao { Id = 2, Descricao = "Sa�da" });
            context.TipoMovimentacoes.AddOrUpdate(new TipoMovimentacao { Id = 3, Descricao = "Devolu��o de Cliente" });
            context.TipoMovimentacoes.AddOrUpdate(new TipoMovimentacao { Id = 4, Descricao = "Devolu��o a Fornecedor" });

            // Estoque
            context.ControleEstoques.AddOrUpdate(new ControleEstoque { Id = 1, BicicletaId = 1, TipoMovimentacaoId = Convert.ToInt32(TipoMovimentacaoEnum.Entrada), Data = new DateTime(2016, 3, 20), Quantidade = 1, Valor = 987, QuantidadeAtual = 1 });
            context.ControleEstoques.AddOrUpdate(new ControleEstoque { Id = 2, BicicletaId = 2, TipoMovimentacaoId = Convert.ToInt32(TipoMovimentacaoEnum.Entrada), Data = new DateTime(2016, 3, 20), Quantidade = 1, Valor = 750, QuantidadeAtual = 1 });

        }
    }
}