namespace EstoqueRepositorioEF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BancoInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bicicletas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Modelo = c.String(nullable: false, maxLength: 50),
                        Preco = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QuantidadeEstoque = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ControleEstoques",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BicicletaId = c.Int(nullable: false),
                        TipoMovimentacaoId = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Quantidade = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QuantidadeAtual = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bicicletas", t => t.BicicletaId, cascadeDelete: false)
                .ForeignKey("dbo.TipoMovimentacoes", t => t.TipoMovimentacaoId, cascadeDelete: false)
                .Index(t => t.BicicletaId)
                .Index(t => t.TipoMovimentacaoId);
            
            CreateTable(
                "dbo.TipoMovimentacoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dominios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Tipo = c.Int(nullable: false),
                        Valor = c.Int(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Gerentes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TipoGerenteId = c.Int(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TipoGerentes", t => t.TipoGerenteId, cascadeDelete: false)
                .Index(t => t.TipoGerenteId);
            
            CreateTable(
                "dbo.TipoGerentes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PrecosPropostos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BicicletaId = c.Int(nullable: false),
                        NovoPreco = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GerenteEstoqueId = c.Int(nullable: false),
                        GerenteLojaOnlineId = c.Int(nullable: false),
                        DataAlterar = c.DateTime(precision: 7, storeType: "datetime2"),
                        HoraAlterar = c.Time(precision: 7),
                        ConfirmadoDataHoraAlteracao = c.Boolean(nullable: false),
                        Alterado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bicicletas", t => t.BicicletaId, cascadeDelete: false)
                .ForeignKey("dbo.Gerentes", t => t.GerenteEstoqueId, cascadeDelete: false)
                .ForeignKey("dbo.Gerentes", t => t.GerenteLojaOnlineId, cascadeDelete: false)
                .Index(t => t.BicicletaId)
                .Index(t => t.GerenteEstoqueId)
                .Index(t => t.GerenteLojaOnlineId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrecosPropostos", "GerenteLojaOnlineId", "dbo.Gerentes");
            DropForeignKey("dbo.PrecosPropostos", "GerenteEstoqueId", "dbo.Gerentes");
            DropForeignKey("dbo.PrecosPropostos", "BicicletaId", "dbo.Bicicletas");
            DropForeignKey("dbo.Gerentes", "TipoGerenteId", "dbo.TipoGerentes");
            DropForeignKey("dbo.ControleEstoques", "TipoMovimentacaoId", "dbo.TipoMovimentacoes");
            DropForeignKey("dbo.ControleEstoques", "BicicletaId", "dbo.Bicicletas");
            DropIndex("dbo.PrecosPropostos", new[] { "GerenteLojaOnlineId" });
            DropIndex("dbo.PrecosPropostos", new[] { "GerenteEstoqueId" });
            DropIndex("dbo.PrecosPropostos", new[] { "BicicletaId" });
            DropIndex("dbo.Gerentes", new[] { "TipoGerenteId" });
            DropIndex("dbo.ControleEstoques", new[] { "TipoMovimentacaoId" });
            DropIndex("dbo.ControleEstoques", new[] { "BicicletaId" });
            DropTable("dbo.PrecosPropostos");
            DropTable("dbo.TipoGerentes");
            DropTable("dbo.Gerentes");
            DropTable("dbo.Dominios");
            DropTable("dbo.TipoMovimentacoes");
            DropTable("dbo.ControleEstoques");
            DropTable("dbo.Bicicletas");
        }
    }
}
