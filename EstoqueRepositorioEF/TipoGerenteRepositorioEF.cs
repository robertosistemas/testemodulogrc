﻿using EstoqueModelo;
using EstoqueRepositorio;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EstoqueRepositorioEF
{
    public class TipoGerenteRepositorioEF : ITipoGerenteRepositorio
    {

        private bool contextoExterno = false;
        private EstoqueDbContext db;

        public TipoGerenteRepositorioEF()
        {
            db = new EstoqueDbContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        public TipoGerenteRepositorioEF(EstoqueDbContext contexto)
        {
            contextoExterno = true;
            db = contexto;
            db.Configuration.ProxyCreationEnabled = false;
        }

        public void incluir(TipoGerente entidade)
        {
            db.TipoGerentes.Add(entidade);
            db.SaveChanges();
        }

        public void atualizar(TipoGerente entidade)
        {
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void excluir(int id)
        {
            TipoGerente tipo = db.TipoGerentes.Find(id);
            db.TipoGerentes.Remove(tipo);
            db.SaveChanges();
        }

        public TipoGerente obterPorId(int id)
        {
            TipoGerente tipo = db.TipoGerentes.Find(id);
            return tipo;
        }

        public List<TipoGerente> obterTodos()
        {
            return db.TipoGerentes.ToList<TipoGerente>();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}