﻿using EstoqueModelo;
using EstoqueRepositorio;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EstoqueRepositorioEF
{
    public class ControleEstoqueRepositorioEF : IControleEstoqueRepositorio
    {

        private bool contextoExterno = false;
        private EstoqueDbContext db;

        public ControleEstoqueRepositorioEF()
        {
            db = new EstoqueDbContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        public ControleEstoqueRepositorioEF(EstoqueDbContext contexto)
        {
            contextoExterno = true;
            db = contexto;
            db.Configuration.ProxyCreationEnabled = false;
        }

        public void incluir(ControleEstoque entidade)
        {
            db.ControleEstoques.Add(entidade);
            db.SaveChanges();
            recalculaSaldosValores(entidade.BicicletaId);
        }

        public void atualizar(ControleEstoque entidade)
        {
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
            recalculaSaldosValores(entidade.BicicletaId);
        }

        public void excluir(int id)
        {
            ControleEstoque esto = db.ControleEstoques.Find(id);
            db.ControleEstoques.Remove(esto);
            db.SaveChanges();
            recalculaSaldosValores(esto.BicicletaId);
        }

        public ControleEstoque obterPorId(int id)
        {
            ControleEstoque esto = db.ControleEstoques.Find(id);
            return esto;
        }

        public List<ControleEstoque> obterTodos()
        {
            return db.ControleEstoques.ToList<ControleEstoque>();
        }

        public List<ControleEstoque> obterPorBicicletaId(int bicicletaId)
        {
            var estos = from m in db.ControleEstoques
                        where m.BicicletaId == bicicletaId
                        select m;
            return estos.ToList<ControleEstoque>();
        }

        public List<ControleEstoque> obterPorPeriodo(DateTime dataInicio, DateTime dataFim)
        {
            var estos = from m in db.ControleEstoques
                        where m.Data >= dataInicio && m.Data <= dataFim
                        select m;
            return estos.ToList<ControleEstoque>();
        }

        public List<ControleEstoque> obterPorBicicletaIdPeriodo(int bicicletaId, DateTime dataInicio, DateTime dataFim)
        {
            var estos = from m in db.ControleEstoques
                        where m.BicicletaId == bicicletaId && m.Data >= dataInicio && m.Data <= dataFim
                        select m;
            return estos.ToList<ControleEstoque>();
        }

        private void atualizarEstoque(ControleEstoque entidade)
        {
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
        }

        private void recalculaSaldosValores(int bicicletaId)
        {
            var estos = from m in db.ControleEstoques
                        where m.BicicletaId == bicicletaId
                        orderby m.Data
                        select m;

            List<ControleEstoque> lista = estos.ToList<ControleEstoque>();

            int Quantidade = 0;
            decimal valor = 0;

            foreach (ControleEstoque item in lista)
            {
                var itemEnum = (TipoMovimentacaoEnum)Enum.Parse(typeof(TipoMovimentacaoEnum), item.TipoMovimentacaoId.ToString());
                switch (itemEnum)
                {
                    case TipoMovimentacaoEnum.Entrada:
                        Quantidade += item.Quantidade;
                        break;
                    case TipoMovimentacaoEnum.Saida:
                        Quantidade -= item.Quantidade;
                        break;
                    case TipoMovimentacaoEnum.DevolucaoCliente:
                        Quantidade += item.Quantidade;
                        break;
                    case TipoMovimentacaoEnum.DevolucaoFonecedor:
                        Quantidade -= item.Quantidade;
                        break;
                    default:
                        break;
                }
                valor = item.Valor;
                item.QuantidadeAtual = Quantidade;

                this.atualizarEstoque(item);
            }

            // Atualizar Bicicleta
            using (BicicletaRepositorioEF biciDb = new BicicletaRepositorioEF(db))
            {
                Bicicleta bi = biciDb.obterPorId(bicicletaId);
                if (bi != null)
                {
                    db.Entry(bi).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}