﻿using EstoqueModelo;
using EstoqueRepositorio;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EstoqueRepositorioEF
{
    public class PrecoPropostoRepositorioEF : IPrecoPropostoRepositorio
    {

        private bool contextoExterno = false;
        private EstoqueDbContext db;

        public PrecoPropostoRepositorioEF()
        {
            db = new EstoqueDbContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        public PrecoPropostoRepositorioEF(EstoqueDbContext contexto)
        {
            contextoExterno = true;
            db = contexto;
            db.Configuration.ProxyCreationEnabled = false;
        }

        public void incluir(PrecoProposto entidade)
        {
            db.PrecosPropostos.Add(entidade);
            db.SaveChanges();
        }

        public void atualizar(PrecoProposto entidade)
        {
            db.PrecosPropostos.Attach(entidade);
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void excluir(int id)
        {
            PrecoProposto prec = db.PrecosPropostos.Find(id);
            db.PrecosPropostos.Remove(prec);
            db.SaveChanges();
        }

        public PrecoProposto obterPorId(int id)
        {
            PrecoProposto prec = db.PrecosPropostos.Find(id);
            return prec;
        }

        public List<PrecoProposto> obterTodos()
        {
            return db.PrecosPropostos.ToList<PrecoProposto>();
        }

        public List<PrecoProposto> obterPorBicicletaId(int bicicletaId)
        {
            var precs = from m in db.PrecosPropostos
                        where m.BicicletaId == bicicletaId
                        select m;
            return precs.ToList<PrecoProposto>();
        }

        public List<PrecoProposto> obterPorGerenteEstoqueId(int gerenteEstoqueId)
        {
            var precs = from m in db.PrecosPropostos
                        where m.GerenteEstoqueId == gerenteEstoqueId
                        select m;
            return precs.ToList<PrecoProposto>();
        }

        public List<PrecoProposto> obterPorGerenteLojaOnlineId(int gerenteLojaOnlineId)
        {
            var precs = from m in db.PrecosPropostos
                        where m.GerenteLojaOnlineId == gerenteLojaOnlineId
                        select m;
            return precs.ToList<PrecoProposto>();
        }

        public List<PrecoProposto> obterRegistroAgendar()
        {
            var precs = from m in db.PrecosPropostos
                        where m.DataAlterar.HasValue == false || m.HoraAlterar.HasValue == false || m.Alterado == false
                        select m;
            return precs.ToList<PrecoProposto>();
        }

        public List<PrecoProposto> obterRegistroProcessar()
        {
            var precs = from m in db.PrecosPropostos
                        where m.DataAlterar.HasValue == true && m.HoraAlterar.HasValue == true && m.ConfirmadoDataHoraAlteracao == true && m.Alterado == false
                        select m;
            return precs.ToList<PrecoProposto>();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}