﻿using EstoqueModelo;
using System;
using System.Data.Entity;
using System.IO;
using System.Configuration;

namespace EstoqueRepositorioEF
{
    public class EstoqueDbContext : DbContext
    {

        static EstoqueDbContext()
        {
            //Database.SetInitializer<EstoqueDbContext>(new EstoqueDbInitializer());
            DbContextUtils<EstoqueDbContext>.SetInitializer(new EstoqueDbInitializer());
        }

        public EstoqueDbContext() : base("EstoqueDbContext")
        {

            //string caminho = ConfigurationManager.AppSettings.Get("AppDataDirectory").ToString();
            //if (string.IsNullOrWhiteSpace(caminho))
            //{
            //    caminho = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "TesteModuloCRC");
            //}
            //if (!System.IO.Directory.Exists(caminho))
            //{
            //    System.IO.Directory.CreateDirectory(caminho);
            //}
            //AppDomain.CurrentDomain.SetData("DataDirectory", caminho);

            // Para serializar as entidades corretamento no serviço WCF
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Dominio> Dominios { get; set; }
        public DbSet<TipoGerente> TipoGerentes { get; set; }
        public DbSet<Gerente> Gerentes { get; set; }
        public DbSet<Bicicleta> Bicicletas { get; set; }
        public DbSet<TipoMovimentacao> TipoMovimentacoes { get; set; }
        public DbSet<ControleEstoque> ControleEstoques { get; set; }
        public DbSet<PrecoProposto> PrecosPropostos { get; set; }

    }
}
