﻿using EstoqueModelo;
using EstoqueRepositorio;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EstoqueRepositorioEF
{
    public class GerenteRepositorioEF : IGerenteRepositorio
    {

        private bool contextoExterno = false;
        private EstoqueDbContext db;

        public GerenteRepositorioEF()
        {
            db = new EstoqueDbContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        public GerenteRepositorioEF(EstoqueDbContext contexto)
        {
            contextoExterno = true;
            db = contexto;
            db.Configuration.ProxyCreationEnabled = false;
        }

        public void incluir(Gerente entidade)
        {
            db.Gerentes.Add(entidade);
            db.SaveChanges();
        }

        public void atualizar(Gerente entidade)
        {
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void excluir(int id)
        {
            Gerente gere = db.Gerentes.Find(id);
            db.Gerentes.Remove(gere);
            db.SaveChanges();
        }

        public Gerente obterPorId(int id)
        {
            Gerente gere = db.Gerentes.Find(id);
            return gere;
        }

        public List<Gerente> obterTodos()
        {
            return db.Gerentes.ToList<Gerente>();
        }

        public List<Gerente> obterPorTipoGerenteId(int tipoGerenteId)
        {
            var geres = from m in db.Gerentes
                        where m.TipoGerenteId == tipoGerenteId
                        select m;
            return geres.ToList<Gerente>();
        }

        public List<Gerente> obterPorNome(string nome)
        {
            var geres = from m in db.Gerentes
                        where m.Nome.Contains(nome)
                        select m;
            return geres.ToList<Gerente>();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}