﻿using EstoqueModelo;
using EstoqueRepositorio;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EstoqueRepositorioEF
{
    public class BicicletaRepositorioEF : IBicicletaRepositorio
    {

        private bool contextoExterno = false;
        private EstoqueDbContext db;

        public BicicletaRepositorioEF()
        {
            db = new EstoqueDbContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        public BicicletaRepositorioEF(EstoqueDbContext contexto)
        {
            contextoExterno = true;
            db = contexto;
            db.Configuration.ProxyCreationEnabled = false;
        }

        public void incluir(Bicicleta entidade)
        {
            db.Bicicletas.Add(entidade);
            db.SaveChanges();
        }

        public void atualizar(Bicicleta entidade)
        {
            //db.Bicicletas.Attach(db.Bicicletas.Single(c => c.Id == entidade.Id));
            //db.Bicicletas.ApplyCurrentValues(entidade);

            //Bicicleta bici = db.Bicicletas.Find(entidade.Id);

            db.Bicicletas.Attach(entidade);
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void excluir(int id)
        {
            Bicicleta bici = db.Bicicletas.Find(id);
            db.Bicicletas.Remove(bici);
            db.SaveChanges();
        }

        public Bicicleta obterPorId(int id)
        {
            Bicicleta bici = db.Bicicletas.Find(id);
            return bici;
        }

        public List<Bicicleta> obterTodos()
        {
            return db.Bicicletas.ToList<Bicicleta>();
        }

        public List<Bicicleta> obterPorModelo(string modelo)
        {
            var bicis = from m in db.Bicicletas
                        where m.Modelo.Equals(modelo)
                        select m;
            return bicis.ToList<Bicicleta>();
        }

        public void atualizarPreco(int id, decimal novoPreco)
        {
            Bicicleta bici = db.Bicicletas.Find(id);
            if (bici != null)
            {
                bici.Preco = novoPreco;
                db.Entry(bici).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void atualizarQuantidadeEstoque(int id, int novaQuantidade)
        {
            Bicicleta bici = db.Bicicletas.Find(id);
            if (bici != null)
            {
                bici.QuantidadeEstoque = novaQuantidade;
                db.Entry(bici).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}