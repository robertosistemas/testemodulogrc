﻿using EstoqueModelo;
using EstoqueRepositorio;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EstoqueRepositorioEF
{
    public class TipoMovimentacaoRepositorioEF : ITipoMovimentacaoRepositorio
    {

        private bool contextoExterno = false;
        private EstoqueDbContext db;

        public TipoMovimentacaoRepositorioEF()
        {
            db = new EstoqueDbContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        public TipoMovimentacaoRepositorioEF(EstoqueDbContext contexto)
        {
            contextoExterno = true;
            db = contexto;
            db.Configuration.ProxyCreationEnabled = false;
        }

        public void incluir(TipoMovimentacao entidade)
        {
            db.TipoMovimentacoes.Add(entidade);
            db.SaveChanges();
        }

        public void atualizar(TipoMovimentacao entidade)
        {
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void excluir(int id)
        {
            TipoMovimentacao tipo = db.TipoMovimentacoes.Find(id);
            db.TipoMovimentacoes.Remove(tipo);
            db.SaveChanges();
        }

        public TipoMovimentacao obterPorId(int id)
        {
            TipoMovimentacao tipo = db.TipoMovimentacoes.Find(id);
            return tipo;
        }

        public List<TipoMovimentacao> obterTodos()
        {
            return db.TipoMovimentacoes.ToList<TipoMovimentacao>();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}