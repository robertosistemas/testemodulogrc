﻿using EstoqueModelo;
using EstoqueRepositorio;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EstoqueRepositorioEF
{
    public class DominioRepositorioEF : IDominioRepositorio
    {

        private bool contextoExterno = false;
        private EstoqueDbContext db;

        public DominioRepositorioEF()
        {
            db = new EstoqueDbContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        public DominioRepositorioEF(EstoqueDbContext contexto)
        {
            contextoExterno = true;
            db = contexto;
            db.Configuration.ProxyCreationEnabled = false;
        }

        public void incluir(Dominio entidade)
        {
            db.Dominios.Add(entidade);
            db.SaveChanges();
        }

        public void atualizar(Dominio entidade)
        {
            db.Entry(entidade).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void excluir(int id)
        {
            Dominio domi = db.Dominios.Find(id);
            db.Dominios.Remove(domi);
            db.SaveChanges();
        }

        public Dominio obterPorId(int id)
        {
            Dominio domi = db.Dominios.Find(id);
            return domi;
        }

        public List<Dominio> obterTodos()
        {
            return db.Dominios.ToList<Dominio>();
        }

        public List<Dominio> obterPorTipo(int tipo)
        {
            var domis = from m in db.Dominios
                        where m.Tipo.Equals(tipo)
                        select m;
            return domis.ToList<Dominio>();
        }

        public List<Dominio> obterPorTipoValor(int tipo, int valor)
        {
            var domis = from m in db.Dominios
                        where m.Tipo == tipo && m.Valor == valor
                        select m;
            return domis.ToList<Dominio>();
        }

        public List<Dominio> obterPorTipoDescricao(int tipo, string descricao)
        {
            var domis = from m in db.Dominios
                        where m.Tipo == tipo && m.Descricao.Contains(descricao)
                        select m;
            return domis.ToList<Dominio>();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (!contextoExterno && db != null)
                    {
                        db.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}