﻿using EstoqueControle.MVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstoqueControle.MVC.Views
{

    [Flags]
    public enum EnumAcaoTela
    {
        Consulta = 0,
        Inclusao = 1,
        Exclusao = 2,
        Alteracao = 4
    }

    public partial class PrecoPropostosMantemForm : Form
    {
        public PrecoPropostosMantemForm()
        {
            InitializeComponent();
            InicializaDados();
        }

        private List<EstoqueModelo.PrecoProposto> _listaDados;

        public List<EstoqueModelo.PrecoProposto> ListaDados { get { return _listaDados; } set { _listaDados = value; } }

        public PrecoPropostosMantemForm(List<EstoqueModelo.PrecoProposto> dados, EnumAcaoTela Acao)
        {
            InitializeComponent();

            if (Acao == EnumAcaoTela.Consulta)
            {
                lblMensagem.Text = "Consulta de preços propostos";
            }

            if (Acao == EnumAcaoTela.Inclusao)
            {
                lblMensagem.Text = "Inclusão de preços propostos";
            }

            if (Acao == EnumAcaoTela.Exclusao)
            {
                lblMensagem.Text = "Confirma exclusão deste registro?";
            }

            if (Acao == EnumAcaoTela.Alteracao)
            {
                lblMensagem.Text = "Alteração de preços propostos";
            }

            ListaDados = dados;

            if (Acao != EnumAcaoTela.Inclusao && ListaDados.Count > 0)
            {
                EstoqueModelo.PrecoProposto item = ListaDados[0];
                if (item.DataAlterar.HasValue)
                {
                    dataAlterarDateTimePicker.Value = item.DataAlterar.Value;
                    dataAlterarDateTimePicker.Checked = true;
                }
                if (item.HoraAlterar.HasValue)
                {
                    horaAlterarmaskedTextBox.Text = item.HoraAlterar.Value.ToString();
                }
            }

            InicializaDados();

        }

        private async void InicializaDados()
        {

            using (AguardeEstado aguardeEstado = new AguardeEstado(AguardeAcaoEnum.Mostrar))
            {
                using (BicicletaServico.BicicletaServicoClient serv = new BicicletaServico.BicicletaServicoClient())
                {
                    bicicletaIdComboBox.DisplayMember = "Modelo";
                    bicicletaIdComboBox.ValueMember = "Id";
                    bicicletaIdComboBox.DataSource = await serv.obterTodosAsync();
                }

                using (GerenteServico.GerenteServicoClient serv = new GerenteServico.GerenteServicoClient())
                {
                    gerenteEstoqueIdComboBox.DisplayMember = "Nome";
                    gerenteEstoqueIdComboBox.ValueMember = "Id";
                    gerenteEstoqueIdComboBox.DataSource = serv.obterTodos();
                }

                using (GerenteServico.GerenteServicoClient serv = new GerenteServico.GerenteServicoClient())
                {
                    gerenteLojaOnlineIdComboBox.DisplayMember = "Nome";
                    gerenteLojaOnlineIdComboBox.ValueMember = "Id";
                    gerenteLojaOnlineIdComboBox.DataSource = await serv.obterTodosAsync();
                }



                //this.gerenteBindingSource.SuspendBinding();
                //this.gerenteBindingSource.RaiseListChangedEvents = false;
                try
                {
                    //bool podefiltrar = this.gerenteBindingSource.SupportsFiltering;
                    //this.gerenteBindingSource.DataSource = bdsGerente.DataSource;
                    this.precoPropostoBindingSource.DataSource = ListaDados;
                    //this.gerenteBindingSource.Filter = String.Format("Id={0}", id);
                }
                finally
                {
                    //this.gerenteBindingSource.ResumeBinding();
                    //this.gerenteBindingSource.RaiseListChangedEvents = true;
                    //this.gerenteBindingSource.ResetBindings(false);
                }

            }
        }

        private void butConfirma_Click(object sender, EventArgs e)
        {
            EstoqueModelo.PrecoProposto item = ListaDados[0];
            if (dataAlterarDateTimePicker.Checked)
            {
                item.DataAlterar = Convert.ToDateTime(dataAlterarDateTimePicker.Value.ToString("dd/MM/yyyy"));
            }
            TimeSpan hora = new TimeSpan();
            if (TimeSpan.TryParse(horaAlterarmaskedTextBox.Text, out hora))
            {
                item.HoraAlterar = hora;
            }
            precoPropostoBindingSource.EndEdit();
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }

        private void butCancela_Click(object sender, EventArgs e)
        {
            precoPropostoBindingSource.CancelEdit();
            this.DialogResult = DialogResult.Cancel;
            this.Hide();
        }
    }
}
