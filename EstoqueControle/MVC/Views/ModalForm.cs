﻿using System.Drawing;
using System.Windows.Forms;

namespace EstoqueControle.MVC.Views
{
    public partial class ModalForm : Form
    {
        public ModalForm(Form frm)
        {
            InitializeComponent();
            mostrarForm(frm);
        }

        private void mostrarForm(Form frm)
        {
            Screen tela = System.Windows.Forms.Screen.PrimaryScreen;
            this.Location = new Point(0, 0);
            this.ClientSize = Screen.PrimaryScreen.WorkingArea.Size;
            this.Show();
            this.Location = new Point(0, 0);
            Controllers.ControladorBase.OcultarAguarde();
            frm.ShowDialog(this);
            this.DialogResult = frm.DialogResult;
            this.Hide();
        }
    }
}
