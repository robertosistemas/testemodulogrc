﻿using EstoqueControle.MVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstoqueControle.MVC.Views
{
    public partial class ControleEstoqueForm : Form
    {
        public ControleEstoqueForm()
        {
            InitializeComponent();
            InicializaDados();
        }

        private async void InicializaDados()
        {
            executandoServicoGerentes = true;
            executandoServicoBicicletas = true;
            executandoServicoPrecosPropostos = true;
            executandoServicoControleEstoques = true;
            try
            {
                using (AguardeEstado aguardeEstado = new AguardeEstado(AguardeAcaoEnum.Mostrar))
                {
                    using (TipoGerenteServico.TipoGerenteServicoClient serv = new TipoGerenteServico.TipoGerenteServicoClient())
                    {
                        tipoGerenteGridBindingSource.DataSource = await serv.obterTodosAsync();
                    }

                    using (BicicletaServico.BicicletaServicoClient serv = new BicicletaServico.BicicletaServicoClient())
                    {
                        bicicletaPrecoGridBindingSource.DataSource = await serv.obterTodosAsync();
                    }

                    using (GerenteServico.GerenteServicoClient serv = new GerenteServico.GerenteServicoClient())
                    {
                        gerenteEstoqueGridBindingSource.DataSource = await serv.obterTodosAsync();
                    }

                    using (GerenteServico.GerenteServicoClient serv = new GerenteServico.GerenteServicoClient())
                    {
                        gerenteLojaGridBindingSource.DataSource = await serv.obterTodosAsync();
                    }

                    using (BicicletaServico.BicicletaServicoClient serv = new BicicletaServico.BicicletaServicoClient())
                    {
                        bicicletaControleGridBindingSource.DataSource = await serv.obterTodosAsync();
                    }

                    using (TipoMovimentacaoServico.TipoMovimentacaoServicoClient serv = new TipoMovimentacaoServico.TipoMovimentacaoServicoClient())
                    {
                        tipoMovimentacaoGridBindingSource.DataSource = await serv.obterTodosAsync();
                    }
                }
            }
            finally
            {
                executandoServicoGerentes = false;
                executandoServicoBicicletas = false;
                executandoServicoPrecosPropostos = false;
                executandoServicoControleEstoques = false;
            }
        }

        private bool executandoServicoGerentes = false;
        private bool executandoServicoBicicletas = false;
        private bool executandoServicoPrecosPropostos = false;
        private bool executandoServicoControleEstoques = false;

        private async void butGerentes_Click(object sender, EventArgs e)
        {
            if (executandoServicoGerentes)
            {
                return;
            }
            try
            {
                executandoServicoGerentes = true;
                using (AguardeEstado aguardeEstado = new AguardeEstado(AguardeAcaoEnum.Mostrar))
                {
                    gerenteBindingSource.DataSource = await Task.Run<List<EstoqueModelo.Gerente>>(() =>
                    {
                        using (GerenteServico.GerenteServicoClient serv = new GerenteServico.GerenteServicoClient())
                        {
                            return serv.obterTodos();
                        }
                    });
                }
                tbcConsultaTabelas.SelectTab(tpgGerentes);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Ocorreu o seguinte Erro: {0}", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                executandoServicoGerentes = false;
            }
        }

        private async void butBicicletas_Click(object sender, EventArgs e)
        {
            if (executandoServicoBicicletas)
            {
                return;
            }
            try
            {
                executandoServicoBicicletas = true;
                using (AguardeEstado aguardeEstado = new AguardeEstado(AguardeAcaoEnum.Mostrar))
                {
                    using (BicicletaServico.BicicletaServicoClient serv = new BicicletaServico.BicicletaServicoClient())
                    {
                        bicicletaBindingSource.DataSource = await serv.obterTodosAsync();
                    }
                }
                tbcConsultaTabelas.SelectTab(tpgBicicletas);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Ocorreu o seguinte Erro: {0}", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                executandoServicoBicicletas = false;
            }
        }

        private async void butPrecosPropostos_Click(object sender, EventArgs e)
        {
            if (executandoServicoPrecosPropostos)
            {
                return;
            }
            try
            {
                executandoServicoPrecosPropostos = true;
                using (AguardeEstado aguardeEstado = new AguardeEstado(AguardeAcaoEnum.Mostrar))
                {
                    using (PrecoPropostoServico.PrecoPropostoServicoClient serv = new PrecoPropostoServico.PrecoPropostoServicoClient())
                    {
                        precoPropostoBindingSource.DataSource = await serv.obterTodosAsync();
                    }
                }
                tbcConsultaTabelas.SelectTab(tpgPrecosPropostos);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Ocorreu o seguinte Erro: {0}", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                executandoServicoPrecosPropostos = false;
            }
        }

        private async void butControleEstoques_Click(object sender, EventArgs e)
        {
            if (executandoServicoControleEstoques)
            {
                return;
            }
            try
            {
                executandoServicoControleEstoques = true;
                using (AguardeEstado aguardeEstado = new AguardeEstado(AguardeAcaoEnum.Mostrar))
                {
                    using (ControleEstoqueServico.ControleEstoqueServicoClient serv = new ControleEstoqueServico.ControleEstoqueServicoClient())
                    {
                        controleEstoqueBindingSource.DataSource = await serv.obterTodosAsync();
                    }
                }
                tbcConsultaTabelas.SelectTab(tpgControleEstoques);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Ocorreu o seguinte Erro: {0}", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                executandoServicoControleEstoques = false;
            }
        }

        private void butIncluirPrecosPropostos_Click(object sender, EventArgs e)
        {
            if (executandoServicoPrecosPropostos)
            {
                return;
            }
            try
            {
                executandoServicoPrecosPropostos = true;
                List<EstoqueModelo.PrecoProposto> lista = new List<EstoqueModelo.PrecoProposto>();
                EstoqueModelo.PrecoProposto item = new EstoqueModelo.PrecoProposto();
                lista.Add(item);
                //precoPropostoBindingSource.AddNew();
                using (PrecoPropostosMantemForm form = new PrecoPropostosMantemForm(lista, EnumAcaoTela.Inclusao))
                {
                    //if (form.ShowDialog() == DialogResult.OK)
                    if (Controllers.ControladorBase.ShowModalForm(form) == DialogResult.OK)
                    {
                        item = form.ListaDados[0];

                        using (PrecoPropostoServico.PrecoPropostoServicoClient serv = new PrecoPropostoServico.PrecoPropostoServicoClient())
                        {
                            serv.incluir(item);
                            precoPropostoBindingSource.DataSource = serv.obterTodos();
                        }

                    }
                    //form.ShowDialog();
                    //this.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Ocorreu o seguinte Erro: {0}", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                executandoServicoPrecosPropostos = false;
            }
        }

        private void butAlterarPrecosPropostos_Click(object sender, EventArgs e)
        {
            if (executandoServicoPrecosPropostos)
            {
                return;
            }
            try
            {
                executandoServicoPrecosPropostos = true;
                List<EstoqueModelo.PrecoProposto> lista = precoPropostoBindingSource.DataSource as List<EstoqueModelo.PrecoProposto>;
                EstoqueModelo.PrecoProposto item = precoPropostoBindingSource.Current as EstoqueModelo.PrecoProposto;
                lista = lista.Where(u => u.Id == item.Id).ToList();
                using (PrecoPropostosMantemForm form = new PrecoPropostosMantemForm(lista, EnumAcaoTela.Alteracao))
                {
                    //if (form.ShowDialog() == DialogResult.OK)
                    if (Controllers.ControladorBase.ShowModalForm(form) == DialogResult.OK)
                    {
                        item = form.ListaDados[0];

                        using (PrecoPropostoServico.PrecoPropostoServicoClient serv = new PrecoPropostoServico.PrecoPropostoServicoClient())
                        {
                            serv.atualizar(item);
                            precoPropostoBindingSource.DataSource = serv.obterTodos();
                        }
                    }
                    //form.ShowDialog();
                    //this.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Ocorreu o seguinte Erro: {0}", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                executandoServicoPrecosPropostos = false;
            }
        }

        private void butExcluirPrecosPropostos_Click(object sender, EventArgs e)
        {
            if (executandoServicoPrecosPropostos)
            {
                return;
            }
            try
            {
                executandoServicoPrecosPropostos = true;
                List<EstoqueModelo.PrecoProposto> lista = precoPropostoBindingSource.DataSource as List<EstoqueModelo.PrecoProposto>;
                EstoqueModelo.PrecoProposto item = precoPropostoBindingSource.Current as EstoqueModelo.PrecoProposto;
                lista = lista.Where(u => u.Id == item.Id).ToList();
                using (PrecoPropostosMantemForm form = new PrecoPropostosMantemForm(lista, EnumAcaoTela.Exclusao))
                {
                    //if (form.ShowDialog() == DialogResult.OK)
                    if (Controllers.ControladorBase.ShowModalForm(form) == DialogResult.OK)
                    {
                        item = form.ListaDados[0];

                        using (PrecoPropostoServico.PrecoPropostoServicoClient serv = new PrecoPropostoServico.PrecoPropostoServicoClient())
                        {
                            serv.excluir(item.Id);
                            precoPropostoBindingSource.DataSource = serv.obterTodos();
                        }
                    }
                    //form.ShowDialog();
                    //this.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Ocorreu o seguinte Erro: {0}", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                executandoServicoPrecosPropostos = false;
            }
        }
    }
}
