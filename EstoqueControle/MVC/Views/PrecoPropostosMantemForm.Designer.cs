﻿namespace EstoqueControle.MVC.Views
{
    partial class PrecoPropostosMantemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label bicicletaIdLabel;
            System.Windows.Forms.Label idLabel;
            System.Windows.Forms.Label novoPrecoLabel;
            System.Windows.Forms.Label gerenteEstoqueIdLabel;
            System.Windows.Forms.Label gerenteLojaOnlineIdLabel;
            this.precoPropostoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bicicletaIdComboBox = new System.Windows.Forms.ComboBox();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.novoPrecoTextBox = new System.Windows.Forms.TextBox();
            this.gerenteEstoqueIdComboBox = new System.Windows.Forms.ComboBox();
            this.gerenteLojaOnlineIdComboBox = new System.Windows.Forms.ComboBox();
            this.dataAlterarlabel = new System.Windows.Forms.Label();
            this.dataAlterarDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.horaAlterarlabel = new System.Windows.Forms.Label();
            this.lblMensagem = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butConfirma = new System.Windows.Forms.Button();
            this.butCancela = new System.Windows.Forms.Button();
            this.horaAlterarmaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            bicicletaIdLabel = new System.Windows.Forms.Label();
            idLabel = new System.Windows.Forms.Label();
            novoPrecoLabel = new System.Windows.Forms.Label();
            gerenteEstoqueIdLabel = new System.Windows.Forms.Label();
            gerenteLojaOnlineIdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.precoPropostoBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bicicletaIdLabel
            // 
            bicicletaIdLabel.AutoSize = true;
            bicicletaIdLabel.Location = new System.Drawing.Point(18, 48);
            bicicletaIdLabel.Name = "bicicletaIdLabel";
            bicicletaIdLabel.Size = new System.Drawing.Size(45, 13);
            bicicletaIdLabel.TabIndex = 2;
            bicicletaIdLabel.Text = "Modelo:";
            // 
            // idLabel
            // 
            idLabel.AutoSize = true;
            idLabel.Location = new System.Drawing.Point(18, 22);
            idLabel.Name = "idLabel";
            idLabel.Size = new System.Drawing.Size(19, 13);
            idLabel.TabIndex = 0;
            idLabel.Text = "Id:";
            // 
            // novoPrecoLabel
            // 
            novoPrecoLabel.AutoSize = true;
            novoPrecoLabel.Location = new System.Drawing.Point(18, 75);
            novoPrecoLabel.Name = "novoPrecoLabel";
            novoPrecoLabel.Size = new System.Drawing.Size(67, 13);
            novoPrecoLabel.TabIndex = 4;
            novoPrecoLabel.Text = "Novo Preco:";
            // 
            // gerenteEstoqueIdLabel
            // 
            gerenteEstoqueIdLabel.AutoSize = true;
            gerenteEstoqueIdLabel.Location = new System.Drawing.Point(18, 101);
            gerenteEstoqueIdLabel.Name = "gerenteEstoqueIdLabel";
            gerenteEstoqueIdLabel.Size = new System.Drawing.Size(90, 13);
            gerenteEstoqueIdLabel.TabIndex = 6;
            gerenteEstoqueIdLabel.Text = "Gerente Estoque:";
            // 
            // gerenteLojaOnlineIdLabel
            // 
            gerenteLojaOnlineIdLabel.AutoSize = true;
            gerenteLojaOnlineIdLabel.Location = new System.Drawing.Point(18, 128);
            gerenteLojaOnlineIdLabel.Name = "gerenteLojaOnlineIdLabel";
            gerenteLojaOnlineIdLabel.Size = new System.Drawing.Size(104, 13);
            gerenteLojaOnlineIdLabel.TabIndex = 8;
            gerenteLojaOnlineIdLabel.Text = "Gerente Loja Online:";
            // 
            // precoPropostoBindingSource
            // 
            this.precoPropostoBindingSource.DataSource = typeof(EstoqueModelo.PrecoProposto);
            // 
            // bicicletaIdComboBox
            // 
            this.bicicletaIdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.precoPropostoBindingSource, "BicicletaId", true));
            this.bicicletaIdComboBox.FormattingEnabled = true;
            this.bicicletaIdComboBox.Location = new System.Drawing.Point(143, 45);
            this.bicicletaIdComboBox.Name = "bicicletaIdComboBox";
            this.bicicletaIdComboBox.Size = new System.Drawing.Size(147, 21);
            this.bicicletaIdComboBox.TabIndex = 3;
            // 
            // idTextBox
            // 
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.precoPropostoBindingSource, "Id", true));
            this.idTextBox.Location = new System.Drawing.Point(143, 19);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.ReadOnly = true;
            this.idTextBox.Size = new System.Drawing.Size(147, 20);
            this.idTextBox.TabIndex = 1;
            // 
            // novoPrecoTextBox
            // 
            this.novoPrecoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.precoPropostoBindingSource, "NovoPreco", true));
            this.novoPrecoTextBox.Location = new System.Drawing.Point(143, 72);
            this.novoPrecoTextBox.Name = "novoPrecoTextBox";
            this.novoPrecoTextBox.Size = new System.Drawing.Size(147, 20);
            this.novoPrecoTextBox.TabIndex = 5;
            // 
            // gerenteEstoqueIdComboBox
            // 
            this.gerenteEstoqueIdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.precoPropostoBindingSource, "GerenteEstoqueId", true));
            this.gerenteEstoqueIdComboBox.FormattingEnabled = true;
            this.gerenteEstoqueIdComboBox.Location = new System.Drawing.Point(143, 98);
            this.gerenteEstoqueIdComboBox.Name = "gerenteEstoqueIdComboBox";
            this.gerenteEstoqueIdComboBox.Size = new System.Drawing.Size(147, 21);
            this.gerenteEstoqueIdComboBox.TabIndex = 7;
            // 
            // gerenteLojaOnlineIdComboBox
            // 
            this.gerenteLojaOnlineIdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.precoPropostoBindingSource, "GerenteLojaOnlineId", true));
            this.gerenteLojaOnlineIdComboBox.FormattingEnabled = true;
            this.gerenteLojaOnlineIdComboBox.Location = new System.Drawing.Point(143, 125);
            this.gerenteLojaOnlineIdComboBox.Name = "gerenteLojaOnlineIdComboBox";
            this.gerenteLojaOnlineIdComboBox.Size = new System.Drawing.Size(147, 21);
            this.gerenteLojaOnlineIdComboBox.TabIndex = 9;
            // 
            // dataAlterarlabel
            // 
            this.dataAlterarlabel.AutoSize = true;
            this.dataAlterarlabel.Location = new System.Drawing.Point(19, 160);
            this.dataAlterarlabel.Name = "dataAlterarlabel";
            this.dataAlterarlabel.Size = new System.Drawing.Size(66, 13);
            this.dataAlterarlabel.TabIndex = 10;
            this.dataAlterarlabel.Text = "Data Alterar:";
            // 
            // dataAlterarDateTimePicker
            // 
            this.dataAlterarDateTimePicker.Checked = false;
            this.dataAlterarDateTimePicker.CustomFormat = "";
            this.dataAlterarDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataAlterarDateTimePicker.Location = new System.Drawing.Point(143, 153);
            this.dataAlterarDateTimePicker.Name = "dataAlterarDateTimePicker";
            this.dataAlterarDateTimePicker.ShowCheckBox = true;
            this.dataAlterarDateTimePicker.Size = new System.Drawing.Size(147, 20);
            this.dataAlterarDateTimePicker.TabIndex = 11;
            // 
            // horaAlterarlabel
            // 
            this.horaAlterarlabel.AutoSize = true;
            this.horaAlterarlabel.Location = new System.Drawing.Point(19, 186);
            this.horaAlterarlabel.Name = "horaAlterarlabel";
            this.horaAlterarlabel.Size = new System.Drawing.Size(66, 13);
            this.horaAlterarlabel.TabIndex = 12;
            this.horaAlterarlabel.Text = "Hora Alterar:";
            // 
            // lblMensagem
            // 
            this.lblMensagem.AutoSize = true;
            this.lblMensagem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensagem.Location = new System.Drawing.Point(20, 9);
            this.lblMensagem.Name = "lblMensagem";
            this.lblMensagem.Size = new System.Drawing.Size(282, 20);
            this.lblMensagem.TabIndex = 0;
            this.lblMensagem.Text = "Confirma exclusão deste registro?";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.horaAlterarmaskedTextBox);
            this.groupBox1.Controls.Add(this.idTextBox);
            this.groupBox1.Controls.Add(this.novoPrecoTextBox);
            this.groupBox1.Controls.Add(novoPrecoLabel);
            this.groupBox1.Controls.Add(this.horaAlterarlabel);
            this.groupBox1.Controls.Add(idLabel);
            this.groupBox1.Controls.Add(this.dataAlterarDateTimePicker);
            this.groupBox1.Controls.Add(this.bicicletaIdComboBox);
            this.groupBox1.Controls.Add(this.dataAlterarlabel);
            this.groupBox1.Controls.Add(bicicletaIdLabel);
            this.groupBox1.Controls.Add(gerenteLojaOnlineIdLabel);
            this.groupBox1.Controls.Add(this.gerenteEstoqueIdComboBox);
            this.groupBox1.Controls.Add(this.gerenteLojaOnlineIdComboBox);
            this.groupBox1.Controls.Add(gerenteEstoqueIdLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(319, 225);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // butConfirma
            // 
            this.butConfirma.Location = new System.Drawing.Point(46, 263);
            this.butConfirma.Name = "butConfirma";
            this.butConfirma.Size = new System.Drawing.Size(115, 25);
            this.butConfirma.TabIndex = 2;
            this.butConfirma.Text = "Confirma";
            this.butConfirma.UseVisualStyleBackColor = true;
            this.butConfirma.Click += new System.EventHandler(this.butConfirma_Click);
            // 
            // butCancela
            // 
            this.butCancela.Location = new System.Drawing.Point(187, 263);
            this.butCancela.Name = "butCancela";
            this.butCancela.Size = new System.Drawing.Size(115, 25);
            this.butCancela.TabIndex = 3;
            this.butCancela.Text = "Cancela";
            this.butCancela.UseVisualStyleBackColor = true;
            this.butCancela.Click += new System.EventHandler(this.butCancela_Click);
            // 
            // horaAlterarmaskedTextBox
            // 
            this.horaAlterarmaskedTextBox.Location = new System.Drawing.Point(143, 179);
            this.horaAlterarmaskedTextBox.Mask = "00:00";
            this.horaAlterarmaskedTextBox.Name = "horaAlterarmaskedTextBox";
            this.horaAlterarmaskedTextBox.Size = new System.Drawing.Size(45, 20);
            this.horaAlterarmaskedTextBox.TabIndex = 13;
            this.horaAlterarmaskedTextBox.ValidatingType = typeof(System.DateTime);
            // 
            // PrecoPropostosMantemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 296);
            this.Controls.Add(this.butCancela);
            this.Controls.Add(this.butConfirma);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblMensagem);
            this.Name = "PrecoPropostosMantemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrecoPropostosMantemForm";
            ((System.ComponentModel.ISupportInitialize)(this.precoPropostoBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource precoPropostoBindingSource;
        private System.Windows.Forms.ComboBox bicicletaIdComboBox;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox novoPrecoTextBox;
        private System.Windows.Forms.ComboBox gerenteEstoqueIdComboBox;
        private System.Windows.Forms.ComboBox gerenteLojaOnlineIdComboBox;
        private System.Windows.Forms.Label dataAlterarlabel;
        private System.Windows.Forms.DateTimePicker dataAlterarDateTimePicker;
        private System.Windows.Forms.Label horaAlterarlabel;
        private System.Windows.Forms.Label lblMensagem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butConfirma;
        private System.Windows.Forms.Button butCancela;
        private System.Windows.Forms.MaskedTextBox horaAlterarmaskedTextBox;
    }
}