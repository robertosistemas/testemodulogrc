﻿using EstoqueControle.MVC.Models;
using EstoqueModelo;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EstoqueControle.MVC.Views
{
    public partial class GerentesMantemForm : Form
    {
        public GerentesMantemForm()
        {
            InitializeComponent();
        }

        public GerentesMantemForm(List<EstoqueModelo.Gerente> dados)
        {
            InitializeComponent();

            //tipoGerenteIdComboBox.DisplayMember = "Descricao";
            //tipoGerenteIdComboBox.ValueMember = "Valor";
            //tipoGerenteIdComboBox.DataSource = EstoqueModelo.Enumeracoes.obterTodosTipoGerente();

            using (AguardeEstado aguardeEstado = new AguardeEstado(AguardeAcaoEnum.Mostrar))
            {
                using (TipoGerenteServico.TipoGerenteServicoClient serv = new TipoGerenteServico.TipoGerenteServicoClient())
                {
                    tipoGerenteIdComboBox.DisplayMember = "Descricao";
                    tipoGerenteIdComboBox.ValueMember = "Id";
                    tipoGerenteIdComboBox.DataSource = serv.obterTodos();
                }
            }

            //this.gerenteBindingSource.SuspendBinding();
            //this.gerenteBindingSource.RaiseListChangedEvents = false;
            try
            {
                //bool podefiltrar = this.gerenteBindingSource.SupportsFiltering;
                //this.gerenteBindingSource.DataSource = bdsGerente.DataSource;
                this.gerenteBindingSource.DataSource = dados;
                //this.gerenteBindingSource.Filter = String.Format("Id={0}", id);
            }
            finally
            {
                //this.gerenteBindingSource.ResumeBinding();
                //this.gerenteBindingSource.RaiseListChangedEvents = true;
                //this.gerenteBindingSource.ResetBindings(false);
            }

        }

        //private bool executandoServicoTipoGerentes = false;

        //private async void butGerentes_Click(object sender, EventArgs e)
        //{
        //    if (executandoServicoTipoGerentes)
        //    {
        //        return;
        //    }
        //    try
        //    {
        //        executandoServicoTipoGerentes = true;
        //        using (AguardeEstado aguardeEstado = new AguardeEstado(AguardeAcaoEnum.Mostrar))
        //        {
        //            using (TipoGerenteServico.TipoGerenteServicoClient serv = new TipoGerenteServico.TipoGerenteServicoClient())
        //            {
        //                tipoGerenteIdComboBox.DataSource = await serv.obterTodosAsync();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(String.Format("Ocorreu o seguinte Erro: {0}", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    finally
        //    {
        //        executandoServicoTipoGerentes = false;
        //    }
        //}

    }
}
