﻿namespace EstoqueControle.MVC.Views
{
    partial class ControleEstoqueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlBotões = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butControleEstoques = new System.Windows.Forms.Button();
            this.butPrecosPropostos = new System.Windows.Forms.Button();
            this.butBicicletas = new System.Windows.Forms.Button();
            this.butGerentes = new System.Windows.Forms.Button();
            this.tbcConsultaTabelas = new System.Windows.Forms.TabControl();
            this.tpgPrecosPropostos = new System.Windows.Forms.TabPage();
            this.dgvPrecosPropostos = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bicicletaIdDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bicicletaPrecoGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bicicletaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.novoPrecoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gerenteEstoqueIdDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.gerenteEstoqueGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gerenteEstoqueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.gerenteLojaGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gerenteLojaOnlineDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataAlterarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraAlterar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfirmadoDataHoraAlteracao = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.alteradoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.precoPropostoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pnlGerentesBotoes = new System.Windows.Forms.Panel();
            this.butExcluirPrecosPropostos = new System.Windows.Forms.Button();
            this.butAlterarPrecosPropostos = new System.Windows.Forms.Button();
            this.butIncluirPrecosPropostos = new System.Windows.Forms.Button();
            this.tpgGerentes = new System.Windows.Forms.TabPage();
            this.dgvGerentes = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoGerenteDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tipoGerenteGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gerenteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tpgBicicletas = new System.Windows.Forms.TabPage();
            this.dgvBicicletas = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modeloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidadeEstoqueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bicicletaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tpgControleEstoques = new System.Windows.Forms.TabPage();
            this.dgvControleEstoques = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bicicletaIdDataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bicicletaControleGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bicicletaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoControleDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tipoMovimentacaoGridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidadeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidadeAtualDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.controleEstoqueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pnlBotões.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tbcConsultaTabelas.SuspendLayout();
            this.tpgPrecosPropostos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrecosPropostos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bicicletaPrecoGridBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gerenteEstoqueGridBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gerenteLojaGridBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.precoPropostoBindingSource)).BeginInit();
            this.pnlGerentesBotoes.SuspendLayout();
            this.tpgGerentes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGerentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoGerenteGridBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gerenteBindingSource)).BeginInit();
            this.tpgBicicletas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBicicletas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bicicletaBindingSource)).BeginInit();
            this.tpgControleEstoques.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvControleEstoques)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bicicletaControleGridBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoMovimentacaoGridBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controleEstoqueBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBotões
            // 
            this.pnlBotões.Controls.Add(this.groupBox1);
            this.pnlBotões.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBotões.Location = new System.Drawing.Point(0, 0);
            this.pnlBotões.Name = "pnlBotões";
            this.pnlBotões.Size = new System.Drawing.Size(843, 59);
            this.pnlBotões.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butControleEstoques);
            this.groupBox1.Controls.Add(this.butPrecosPropostos);
            this.groupBox1.Controls.Add(this.butBicicletas);
            this.groupBox1.Controls.Add(this.butGerentes);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(843, 59);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consultas";
            // 
            // butControleEstoques
            // 
            this.butControleEstoques.Location = new System.Drawing.Point(337, 19);
            this.butControleEstoques.Name = "butControleEstoques";
            this.butControleEstoques.Size = new System.Drawing.Size(101, 30);
            this.butControleEstoques.TabIndex = 3;
            this.butControleEstoques.Text = "Controle Estoques";
            this.butControleEstoques.UseVisualStyleBackColor = true;
            this.butControleEstoques.Click += new System.EventHandler(this.butControleEstoques_Click);
            // 
            // butPrecosPropostos
            // 
            this.butPrecosPropostos.Location = new System.Drawing.Point(16, 19);
            this.butPrecosPropostos.Name = "butPrecosPropostos";
            this.butPrecosPropostos.Size = new System.Drawing.Size(101, 30);
            this.butPrecosPropostos.TabIndex = 0;
            this.butPrecosPropostos.Text = "Precos propostos";
            this.butPrecosPropostos.UseVisualStyleBackColor = true;
            this.butPrecosPropostos.Click += new System.EventHandler(this.butPrecosPropostos_Click);
            // 
            // butBicicletas
            // 
            this.butBicicletas.Location = new System.Drawing.Point(230, 19);
            this.butBicicletas.Name = "butBicicletas";
            this.butBicicletas.Size = new System.Drawing.Size(101, 30);
            this.butBicicletas.TabIndex = 2;
            this.butBicicletas.Text = "Bicicletas";
            this.butBicicletas.UseVisualStyleBackColor = true;
            this.butBicicletas.Click += new System.EventHandler(this.butBicicletas_Click);
            // 
            // butGerentes
            // 
            this.butGerentes.Location = new System.Drawing.Point(123, 19);
            this.butGerentes.Name = "butGerentes";
            this.butGerentes.Size = new System.Drawing.Size(101, 30);
            this.butGerentes.TabIndex = 1;
            this.butGerentes.Text = "Gerentes";
            this.butGerentes.UseVisualStyleBackColor = true;
            this.butGerentes.Click += new System.EventHandler(this.butGerentes_Click);
            // 
            // tbcConsultaTabelas
            // 
            this.tbcConsultaTabelas.Controls.Add(this.tpgPrecosPropostos);
            this.tbcConsultaTabelas.Controls.Add(this.tpgGerentes);
            this.tbcConsultaTabelas.Controls.Add(this.tpgBicicletas);
            this.tbcConsultaTabelas.Controls.Add(this.tpgControleEstoques);
            this.tbcConsultaTabelas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcConsultaTabelas.Location = new System.Drawing.Point(0, 59);
            this.tbcConsultaTabelas.Name = "tbcConsultaTabelas";
            this.tbcConsultaTabelas.SelectedIndex = 0;
            this.tbcConsultaTabelas.Size = new System.Drawing.Size(843, 323);
            this.tbcConsultaTabelas.TabIndex = 0;
            // 
            // tpgPrecosPropostos
            // 
            this.tpgPrecosPropostos.Controls.Add(this.dgvPrecosPropostos);
            this.tpgPrecosPropostos.Controls.Add(this.pnlGerentesBotoes);
            this.tpgPrecosPropostos.Location = new System.Drawing.Point(4, 22);
            this.tpgPrecosPropostos.Name = "tpgPrecosPropostos";
            this.tpgPrecosPropostos.Padding = new System.Windows.Forms.Padding(3);
            this.tpgPrecosPropostos.Size = new System.Drawing.Size(835, 297);
            this.tpgPrecosPropostos.TabIndex = 2;
            this.tpgPrecosPropostos.Text = "Precos propostos";
            this.tpgPrecosPropostos.UseVisualStyleBackColor = true;
            // 
            // dgvPrecosPropostos
            // 
            this.dgvPrecosPropostos.AllowUserToAddRows = false;
            this.dgvPrecosPropostos.AllowUserToDeleteRows = false;
            this.dgvPrecosPropostos.AutoGenerateColumns = false;
            this.dgvPrecosPropostos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrecosPropostos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.bicicletaIdDataGridViewComboBoxColumn,
            this.bicicletaDataGridViewTextBoxColumn,
            this.novoPrecoDataGridViewTextBoxColumn,
            this.gerenteEstoqueIdDataGridViewComboBoxColumn,
            this.gerenteEstoqueDataGridViewTextBoxColumn,
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn,
            this.gerenteLojaOnlineDataGridViewTextBoxColumn,
            this.dataAlterarDataGridViewTextBoxColumn,
            this.HoraAlterar,
            this.ConfirmadoDataHoraAlteracao,
            this.alteradoDataGridViewCheckBoxColumn});
            this.dgvPrecosPropostos.DataSource = this.precoPropostoBindingSource;
            this.dgvPrecosPropostos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPrecosPropostos.Location = new System.Drawing.Point(3, 3);
            this.dgvPrecosPropostos.Name = "dgvPrecosPropostos";
            this.dgvPrecosPropostos.ReadOnly = true;
            this.dgvPrecosPropostos.Size = new System.Drawing.Size(718, 291);
            this.dgvPrecosPropostos.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.ReadOnly = true;
            this.idDataGridViewTextBoxColumn2.Width = 50;
            // 
            // bicicletaIdDataGridViewComboBoxColumn
            // 
            this.bicicletaIdDataGridViewComboBoxColumn.DataPropertyName = "BicicletaId";
            this.bicicletaIdDataGridViewComboBoxColumn.DataSource = this.bicicletaPrecoGridBindingSource;
            this.bicicletaIdDataGridViewComboBoxColumn.DisplayMember = "Modelo";
            this.bicicletaIdDataGridViewComboBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.bicicletaIdDataGridViewComboBoxColumn.HeaderText = "Bicicleta";
            this.bicicletaIdDataGridViewComboBoxColumn.Name = "bicicletaIdDataGridViewComboBoxColumn";
            this.bicicletaIdDataGridViewComboBoxColumn.ReadOnly = true;
            this.bicicletaIdDataGridViewComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bicicletaIdDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.bicicletaIdDataGridViewComboBoxColumn.ValueMember = "Id";
            this.bicicletaIdDataGridViewComboBoxColumn.Width = 150;
            // 
            // bicicletaPrecoGridBindingSource
            // 
            this.bicicletaPrecoGridBindingSource.DataSource = typeof(EstoqueModelo.Bicicleta);
            // 
            // bicicletaDataGridViewTextBoxColumn
            // 
            this.bicicletaDataGridViewTextBoxColumn.DataPropertyName = "Bicicleta";
            this.bicicletaDataGridViewTextBoxColumn.HeaderText = "Bicicleta";
            this.bicicletaDataGridViewTextBoxColumn.Name = "bicicletaDataGridViewTextBoxColumn";
            this.bicicletaDataGridViewTextBoxColumn.ReadOnly = true;
            this.bicicletaDataGridViewTextBoxColumn.Visible = false;
            // 
            // novoPrecoDataGridViewTextBoxColumn
            // 
            this.novoPrecoDataGridViewTextBoxColumn.DataPropertyName = "NovoPreco";
            this.novoPrecoDataGridViewTextBoxColumn.HeaderText = "Novo Preço";
            this.novoPrecoDataGridViewTextBoxColumn.Name = "novoPrecoDataGridViewTextBoxColumn";
            this.novoPrecoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // gerenteEstoqueIdDataGridViewComboBoxColumn
            // 
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.DataPropertyName = "GerenteEstoqueId";
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.DataSource = this.gerenteEstoqueGridBindingSource;
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.DisplayMember = "Nome";
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.HeaderText = "Gerente Estoque";
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.Name = "gerenteEstoqueIdDataGridViewComboBoxColumn";
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.ReadOnly = true;
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.ValueMember = "Id";
            this.gerenteEstoqueIdDataGridViewComboBoxColumn.Width = 150;
            // 
            // gerenteEstoqueGridBindingSource
            // 
            this.gerenteEstoqueGridBindingSource.DataSource = typeof(EstoqueModelo.Gerente);
            // 
            // gerenteEstoqueDataGridViewTextBoxColumn
            // 
            this.gerenteEstoqueDataGridViewTextBoxColumn.DataPropertyName = "GerenteEstoque";
            this.gerenteEstoqueDataGridViewTextBoxColumn.HeaderText = "GerenteEstoque";
            this.gerenteEstoqueDataGridViewTextBoxColumn.Name = "gerenteEstoqueDataGridViewTextBoxColumn";
            this.gerenteEstoqueDataGridViewTextBoxColumn.ReadOnly = true;
            this.gerenteEstoqueDataGridViewTextBoxColumn.Visible = false;
            // 
            // gerenteLojaOnlineIdDataGridViewComboBoxColumn
            // 
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.DataPropertyName = "GerenteLojaOnlineId";
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.DataSource = this.gerenteLojaGridBindingSource;
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.DisplayMember = "Nome";
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.HeaderText = "Gerente Loja Online";
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.Name = "gerenteLojaOnlineIdDataGridViewComboBoxColumn";
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.ReadOnly = true;
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.ValueMember = "Id";
            this.gerenteLojaOnlineIdDataGridViewComboBoxColumn.Width = 150;
            // 
            // gerenteLojaGridBindingSource
            // 
            this.gerenteLojaGridBindingSource.DataSource = typeof(EstoqueModelo.Gerente);
            // 
            // gerenteLojaOnlineDataGridViewTextBoxColumn
            // 
            this.gerenteLojaOnlineDataGridViewTextBoxColumn.DataPropertyName = "GerenteLojaOnline";
            this.gerenteLojaOnlineDataGridViewTextBoxColumn.HeaderText = "GerenteLojaOnline";
            this.gerenteLojaOnlineDataGridViewTextBoxColumn.Name = "gerenteLojaOnlineDataGridViewTextBoxColumn";
            this.gerenteLojaOnlineDataGridViewTextBoxColumn.ReadOnly = true;
            this.gerenteLojaOnlineDataGridViewTextBoxColumn.Visible = false;
            // 
            // dataAlterarDataGridViewTextBoxColumn
            // 
            this.dataAlterarDataGridViewTextBoxColumn.DataPropertyName = "DataAlterar";
            this.dataAlterarDataGridViewTextBoxColumn.HeaderText = "Data Alterar";
            this.dataAlterarDataGridViewTextBoxColumn.Name = "dataAlterarDataGridViewTextBoxColumn";
            this.dataAlterarDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // HoraAlterar
            // 
            this.HoraAlterar.DataPropertyName = "HoraAlterar";
            this.HoraAlterar.HeaderText = "Hora alterar";
            this.HoraAlterar.Name = "HoraAlterar";
            this.HoraAlterar.ReadOnly = true;
            // 
            // ConfirmadoDataHoraAlteracao
            // 
            this.ConfirmadoDataHoraAlteracao.DataPropertyName = "ConfirmadoDataHoraAlteracao";
            this.ConfirmadoDataHoraAlteracao.HeaderText = "Confirmado";
            this.ConfirmadoDataHoraAlteracao.Name = "ConfirmadoDataHoraAlteracao";
            this.ConfirmadoDataHoraAlteracao.ReadOnly = true;
            // 
            // alteradoDataGridViewCheckBoxColumn
            // 
            this.alteradoDataGridViewCheckBoxColumn.DataPropertyName = "Alterado";
            this.alteradoDataGridViewCheckBoxColumn.HeaderText = "Alterado";
            this.alteradoDataGridViewCheckBoxColumn.Name = "alteradoDataGridViewCheckBoxColumn";
            this.alteradoDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // precoPropostoBindingSource
            // 
            this.precoPropostoBindingSource.DataSource = typeof(EstoqueModelo.PrecoProposto);
            // 
            // pnlGerentesBotoes
            // 
            this.pnlGerentesBotoes.Controls.Add(this.butExcluirPrecosPropostos);
            this.pnlGerentesBotoes.Controls.Add(this.butAlterarPrecosPropostos);
            this.pnlGerentesBotoes.Controls.Add(this.butIncluirPrecosPropostos);
            this.pnlGerentesBotoes.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlGerentesBotoes.Location = new System.Drawing.Point(721, 3);
            this.pnlGerentesBotoes.Name = "pnlGerentesBotoes";
            this.pnlGerentesBotoes.Size = new System.Drawing.Size(111, 291);
            this.pnlGerentesBotoes.TabIndex = 1;
            // 
            // butExcluirPrecosPropostos
            // 
            this.butExcluirPrecosPropostos.Location = new System.Drawing.Point(8, 92);
            this.butExcluirPrecosPropostos.Name = "butExcluirPrecosPropostos";
            this.butExcluirPrecosPropostos.Size = new System.Drawing.Size(101, 30);
            this.butExcluirPrecosPropostos.TabIndex = 2;
            this.butExcluirPrecosPropostos.Text = "Excluir";
            this.butExcluirPrecosPropostos.UseVisualStyleBackColor = true;
            this.butExcluirPrecosPropostos.Click += new System.EventHandler(this.butExcluirPrecosPropostos_Click);
            // 
            // butAlterarPrecosPropostos
            // 
            this.butAlterarPrecosPropostos.Location = new System.Drawing.Point(7, 56);
            this.butAlterarPrecosPropostos.Name = "butAlterarPrecosPropostos";
            this.butAlterarPrecosPropostos.Size = new System.Drawing.Size(101, 30);
            this.butAlterarPrecosPropostos.TabIndex = 1;
            this.butAlterarPrecosPropostos.Text = "Alterar";
            this.butAlterarPrecosPropostos.UseVisualStyleBackColor = true;
            this.butAlterarPrecosPropostos.Click += new System.EventHandler(this.butAlterarPrecosPropostos_Click);
            // 
            // butIncluirPrecosPropostos
            // 
            this.butIncluirPrecosPropostos.Location = new System.Drawing.Point(7, 20);
            this.butIncluirPrecosPropostos.Name = "butIncluirPrecosPropostos";
            this.butIncluirPrecosPropostos.Size = new System.Drawing.Size(101, 30);
            this.butIncluirPrecosPropostos.TabIndex = 0;
            this.butIncluirPrecosPropostos.Text = "Incluir";
            this.butIncluirPrecosPropostos.UseVisualStyleBackColor = true;
            this.butIncluirPrecosPropostos.Click += new System.EventHandler(this.butIncluirPrecosPropostos_Click);
            // 
            // tpgGerentes
            // 
            this.tpgGerentes.Controls.Add(this.dgvGerentes);
            this.tpgGerentes.Location = new System.Drawing.Point(4, 22);
            this.tpgGerentes.Name = "tpgGerentes";
            this.tpgGerentes.Padding = new System.Windows.Forms.Padding(3);
            this.tpgGerentes.Size = new System.Drawing.Size(835, 297);
            this.tpgGerentes.TabIndex = 0;
            this.tpgGerentes.Text = "Gerentes";
            this.tpgGerentes.UseVisualStyleBackColor = true;
            // 
            // dgvGerentes
            // 
            this.dgvGerentes.AllowUserToAddRows = false;
            this.dgvGerentes.AllowUserToDeleteRows = false;
            this.dgvGerentes.AutoGenerateColumns = false;
            this.dgvGerentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGerentes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.tipoGerenteDataGridViewComboBoxColumn,
            this.nomeDataGridViewTextBoxColumn});
            this.dgvGerentes.DataSource = this.gerenteBindingSource;
            this.dgvGerentes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGerentes.Location = new System.Drawing.Point(3, 3);
            this.dgvGerentes.Name = "dgvGerentes";
            this.dgvGerentes.ReadOnly = true;
            this.dgvGerentes.Size = new System.Drawing.Size(829, 291);
            this.dgvGerentes.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Width = 50;
            // 
            // tipoGerenteDataGridViewComboBoxColumn
            // 
            this.tipoGerenteDataGridViewComboBoxColumn.DataPropertyName = "TipoGerenteId";
            this.tipoGerenteDataGridViewComboBoxColumn.DataSource = this.tipoGerenteGridBindingSource;
            this.tipoGerenteDataGridViewComboBoxColumn.DisplayMember = "Descricao";
            this.tipoGerenteDataGridViewComboBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.tipoGerenteDataGridViewComboBoxColumn.HeaderText = "Tipo";
            this.tipoGerenteDataGridViewComboBoxColumn.Name = "tipoGerenteDataGridViewComboBoxColumn";
            this.tipoGerenteDataGridViewComboBoxColumn.ReadOnly = true;
            this.tipoGerenteDataGridViewComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tipoGerenteDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tipoGerenteDataGridViewComboBoxColumn.ValueMember = "Id";
            this.tipoGerenteDataGridViewComboBoxColumn.Width = 200;
            // 
            // tipoGerenteGridBindingSource
            // 
            this.tipoGerenteGridBindingSource.DataSource = typeof(EstoqueModelo.TipoGerente);
            // 
            // nomeDataGridViewTextBoxColumn
            // 
            this.nomeDataGridViewTextBoxColumn.DataPropertyName = "Nome";
            this.nomeDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nomeDataGridViewTextBoxColumn.Name = "nomeDataGridViewTextBoxColumn";
            this.nomeDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomeDataGridViewTextBoxColumn.Width = 200;
            // 
            // gerenteBindingSource
            // 
            this.gerenteBindingSource.DataSource = typeof(EstoqueModelo.Gerente);
            // 
            // tpgBicicletas
            // 
            this.tpgBicicletas.Controls.Add(this.dgvBicicletas);
            this.tpgBicicletas.Location = new System.Drawing.Point(4, 22);
            this.tpgBicicletas.Name = "tpgBicicletas";
            this.tpgBicicletas.Padding = new System.Windows.Forms.Padding(3);
            this.tpgBicicletas.Size = new System.Drawing.Size(835, 297);
            this.tpgBicicletas.TabIndex = 1;
            this.tpgBicicletas.Text = "Bicicletas";
            this.tpgBicicletas.UseVisualStyleBackColor = true;
            // 
            // dgvBicicletas
            // 
            this.dgvBicicletas.AllowUserToAddRows = false;
            this.dgvBicicletas.AllowUserToDeleteRows = false;
            this.dgvBicicletas.AutoGenerateColumns = false;
            this.dgvBicicletas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBicicletas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.modeloDataGridViewTextBoxColumn,
            this.precoDataGridViewTextBoxColumn,
            this.quantidadeEstoqueDataGridViewTextBoxColumn});
            this.dgvBicicletas.DataSource = this.bicicletaBindingSource;
            this.dgvBicicletas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBicicletas.Location = new System.Drawing.Point(3, 3);
            this.dgvBicicletas.Name = "dgvBicicletas";
            this.dgvBicicletas.ReadOnly = true;
            this.dgvBicicletas.Size = new System.Drawing.Size(829, 291);
            this.dgvBicicletas.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Width = 50;
            // 
            // modeloDataGridViewTextBoxColumn
            // 
            this.modeloDataGridViewTextBoxColumn.DataPropertyName = "Modelo";
            this.modeloDataGridViewTextBoxColumn.HeaderText = "Modelo";
            this.modeloDataGridViewTextBoxColumn.Name = "modeloDataGridViewTextBoxColumn";
            this.modeloDataGridViewTextBoxColumn.ReadOnly = true;
            this.modeloDataGridViewTextBoxColumn.Width = 200;
            // 
            // precoDataGridViewTextBoxColumn
            // 
            this.precoDataGridViewTextBoxColumn.DataPropertyName = "Preco";
            this.precoDataGridViewTextBoxColumn.HeaderText = "Preço";
            this.precoDataGridViewTextBoxColumn.Name = "precoDataGridViewTextBoxColumn";
            this.precoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantidadeEstoqueDataGridViewTextBoxColumn
            // 
            this.quantidadeEstoqueDataGridViewTextBoxColumn.DataPropertyName = "QuantidadeEstoque";
            this.quantidadeEstoqueDataGridViewTextBoxColumn.HeaderText = "Qtd. Estoque";
            this.quantidadeEstoqueDataGridViewTextBoxColumn.Name = "quantidadeEstoqueDataGridViewTextBoxColumn";
            this.quantidadeEstoqueDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bicicletaBindingSource
            // 
            this.bicicletaBindingSource.DataSource = typeof(EstoqueModelo.Bicicleta);
            // 
            // tpgControleEstoques
            // 
            this.tpgControleEstoques.Controls.Add(this.dgvControleEstoques);
            this.tpgControleEstoques.Location = new System.Drawing.Point(4, 22);
            this.tpgControleEstoques.Name = "tpgControleEstoques";
            this.tpgControleEstoques.Padding = new System.Windows.Forms.Padding(3);
            this.tpgControleEstoques.Size = new System.Drawing.Size(835, 297);
            this.tpgControleEstoques.TabIndex = 3;
            this.tpgControleEstoques.Text = "Controle estoques";
            this.tpgControleEstoques.UseVisualStyleBackColor = true;
            // 
            // dgvControleEstoques
            // 
            this.dgvControleEstoques.AllowUserToAddRows = false;
            this.dgvControleEstoques.AllowUserToDeleteRows = false;
            this.dgvControleEstoques.AutoGenerateColumns = false;
            this.dgvControleEstoques.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvControleEstoques.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn3,
            this.bicicletaIdDataGridViewComboBoxColumn1,
            this.bicicletaDataGridViewTextBoxColumn1,
            this.tipoControleDataGridViewComboBoxColumn,
            this.dataDataGridViewTextBoxColumn,
            this.quantidadeDataGridViewTextBoxColumn,
            this.valorDataGridViewTextBoxColumn,
            this.quantidadeAtualDataGridViewTextBoxColumn});
            this.dgvControleEstoques.DataSource = this.controleEstoqueBindingSource;
            this.dgvControleEstoques.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvControleEstoques.Location = new System.Drawing.Point(3, 3);
            this.dgvControleEstoques.Name = "dgvControleEstoques";
            this.dgvControleEstoques.ReadOnly = true;
            this.dgvControleEstoques.Size = new System.Drawing.Size(829, 291);
            this.dgvControleEstoques.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn3
            // 
            this.idDataGridViewTextBoxColumn3.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn3.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn3.Name = "idDataGridViewTextBoxColumn3";
            this.idDataGridViewTextBoxColumn3.ReadOnly = true;
            this.idDataGridViewTextBoxColumn3.Width = 50;
            // 
            // bicicletaIdDataGridViewComboBoxColumn1
            // 
            this.bicicletaIdDataGridViewComboBoxColumn1.DataPropertyName = "BicicletaId";
            this.bicicletaIdDataGridViewComboBoxColumn1.DataSource = this.bicicletaControleGridBindingSource;
            this.bicicletaIdDataGridViewComboBoxColumn1.DisplayMember = "Modelo";
            this.bicicletaIdDataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.bicicletaIdDataGridViewComboBoxColumn1.HeaderText = "Bicicleta";
            this.bicicletaIdDataGridViewComboBoxColumn1.Name = "bicicletaIdDataGridViewComboBoxColumn1";
            this.bicicletaIdDataGridViewComboBoxColumn1.ReadOnly = true;
            this.bicicletaIdDataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.bicicletaIdDataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.bicicletaIdDataGridViewComboBoxColumn1.ValueMember = "Id";
            this.bicicletaIdDataGridViewComboBoxColumn1.Width = 150;
            // 
            // bicicletaControleGridBindingSource
            // 
            this.bicicletaControleGridBindingSource.DataSource = typeof(EstoqueModelo.Bicicleta);
            // 
            // bicicletaDataGridViewTextBoxColumn1
            // 
            this.bicicletaDataGridViewTextBoxColumn1.DataPropertyName = "Bicicleta";
            this.bicicletaDataGridViewTextBoxColumn1.HeaderText = "Bicicleta";
            this.bicicletaDataGridViewTextBoxColumn1.Name = "bicicletaDataGridViewTextBoxColumn1";
            this.bicicletaDataGridViewTextBoxColumn1.ReadOnly = true;
            this.bicicletaDataGridViewTextBoxColumn1.Visible = false;
            // 
            // tipoControleDataGridViewComboBoxColumn
            // 
            this.tipoControleDataGridViewComboBoxColumn.DataPropertyName = "TipoMovimentacaoId";
            this.tipoControleDataGridViewComboBoxColumn.DataSource = this.tipoMovimentacaoGridBindingSource;
            this.tipoControleDataGridViewComboBoxColumn.DisplayMember = "Descricao";
            this.tipoControleDataGridViewComboBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.tipoControleDataGridViewComboBoxColumn.HeaderText = "Tipo";
            this.tipoControleDataGridViewComboBoxColumn.Name = "tipoControleDataGridViewComboBoxColumn";
            this.tipoControleDataGridViewComboBoxColumn.ReadOnly = true;
            this.tipoControleDataGridViewComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tipoControleDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.tipoControleDataGridViewComboBoxColumn.ValueMember = "Id";
            // 
            // tipoMovimentacaoGridBindingSource
            // 
            this.tipoMovimentacaoGridBindingSource.DataSource = typeof(EstoqueModelo.TipoMovimentacao);
            // 
            // dataDataGridViewTextBoxColumn
            // 
            this.dataDataGridViewTextBoxColumn.DataPropertyName = "Data";
            this.dataDataGridViewTextBoxColumn.HeaderText = "Data";
            this.dataDataGridViewTextBoxColumn.Name = "dataDataGridViewTextBoxColumn";
            this.dataDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantidadeDataGridViewTextBoxColumn
            // 
            this.quantidadeDataGridViewTextBoxColumn.DataPropertyName = "Quantidade";
            this.quantidadeDataGridViewTextBoxColumn.HeaderText = "Quantidade";
            this.quantidadeDataGridViewTextBoxColumn.Name = "quantidadeDataGridViewTextBoxColumn";
            this.quantidadeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // valorDataGridViewTextBoxColumn
            // 
            this.valorDataGridViewTextBoxColumn.DataPropertyName = "Valor";
            this.valorDataGridViewTextBoxColumn.HeaderText = "Valor";
            this.valorDataGridViewTextBoxColumn.Name = "valorDataGridViewTextBoxColumn";
            this.valorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantidadeAtualDataGridViewTextBoxColumn
            // 
            this.quantidadeAtualDataGridViewTextBoxColumn.DataPropertyName = "QuantidadeAtual";
            this.quantidadeAtualDataGridViewTextBoxColumn.HeaderText = "Qtd. Atual";
            this.quantidadeAtualDataGridViewTextBoxColumn.Name = "quantidadeAtualDataGridViewTextBoxColumn";
            this.quantidadeAtualDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // controleEstoqueBindingSource
            // 
            this.controleEstoqueBindingSource.DataSource = typeof(EstoqueModelo.ControleEstoque);
            // 
            // ControleEstoqueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 382);
            this.Controls.Add(this.tbcConsultaTabelas);
            this.Controls.Add(this.pnlBotões);
            this.Name = "ControleEstoqueForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConsultaTabelasForm";
            this.pnlBotões.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tbcConsultaTabelas.ResumeLayout(false);
            this.tpgPrecosPropostos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrecosPropostos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bicicletaPrecoGridBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gerenteEstoqueGridBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gerenteLojaGridBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.precoPropostoBindingSource)).EndInit();
            this.pnlGerentesBotoes.ResumeLayout(false);
            this.tpgGerentes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGerentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoGerenteGridBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gerenteBindingSource)).EndInit();
            this.tpgBicicletas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBicicletas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bicicletaBindingSource)).EndInit();
            this.tpgControleEstoques.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvControleEstoques)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bicicletaControleGridBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoMovimentacaoGridBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controleEstoqueBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBotões;
        private System.Windows.Forms.TabControl tbcConsultaTabelas;
        private System.Windows.Forms.TabPage tpgGerentes;
        private System.Windows.Forms.TabPage tpgBicicletas;
        private System.Windows.Forms.TabPage tpgPrecosPropostos;
        private System.Windows.Forms.TabPage tpgControleEstoques;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butBicicletas;
        private System.Windows.Forms.Button butGerentes;
        private System.Windows.Forms.Button butPrecosPropostos;
        private System.Windows.Forms.Button butControleEstoques;
        private System.Windows.Forms.DataGridView dgvGerentes;
        private System.Windows.Forms.DataGridView dgvBicicletas;
        private System.Windows.Forms.DataGridView dgvPrecosPropostos;
        private System.Windows.Forms.DataGridView dgvControleEstoques;
        private System.Windows.Forms.BindingSource gerenteBindingSource;
        private System.Windows.Forms.BindingSource bicicletaBindingSource;
        private System.Windows.Forms.BindingSource precoPropostoBindingSource;
        private System.Windows.Forms.BindingSource controleEstoqueBindingSource;
        private System.Windows.Forms.BindingSource tipoGerenteGridBindingSource;
        private System.Windows.Forms.BindingSource tipoMovimentacaoGridBindingSource;
        private System.Windows.Forms.BindingSource bicicletaPrecoGridBindingSource;
        private System.Windows.Forms.BindingSource gerenteEstoqueGridBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn modeloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn precoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantidadeEstoqueDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource gerenteLojaGridBindingSource;
        private System.Windows.Forms.BindingSource bicicletaControleGridBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn tipoGerenteDataGridViewComboBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn bicicletaIdDataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn bicicletaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn tipoControleDataGridViewComboBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantidadeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantidadeAtualDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn bicicletaIdDataGridViewComboBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bicicletaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn novoPrecoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn gerenteEstoqueIdDataGridViewComboBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gerenteEstoqueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn gerenteLojaOnlineIdDataGridViewComboBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gerenteLojaOnlineDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataAlterarDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraAlterar;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ConfirmadoDataHoraAlteracao;
        private System.Windows.Forms.DataGridViewCheckBoxColumn alteradoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.Panel pnlGerentesBotoes;
        private System.Windows.Forms.Button butExcluirPrecosPropostos;
        private System.Windows.Forms.Button butAlterarPrecosPropostos;
        private System.Windows.Forms.Button butIncluirPrecosPropostos;
    }
}