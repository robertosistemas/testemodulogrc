﻿using System.Windows.Forms;

namespace EstoqueControle.MVC.Views
{
    public partial class AguardeForm : Form
    {
        public AguardeForm()
        {
            InitializeComponent();
            PodeMostrar = true;
            PodeOcultar = true;
        }

        //static AguardeForm()
        //{
        //    PodeMostrar = true;
        //    PodeOcultar = true;
        //}

        public static bool PodeMostrar { get; set; }
        public static bool PodeOcultar { get; set; }

        public void Mostrar()
        {
            if (PodeMostrar)
            {
                this.Show();
                this.Refresh();
            }
        }

        public void Ocultar()
        {
            if (PodeOcultar)
            {
                this.Hide();
            }
        }

    }
}
