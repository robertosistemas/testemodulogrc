﻿using EstoqueControle.MVC.Controllers;
using EstoqueControle.MVC.Views;
using System;
using System.Windows.Forms;

namespace EstoqueControle.MVC.Models
{

    public enum AguardeAcaoEnum
    {
        Mostrar = 1,
        Ocultar = 2
    }

    public class AguardeEstado : IDisposable
    {

        private bool Visivel { get; set; }
        private bool PodeMostrar { get; set; }
        private bool PodeOcultar { get; set; }

        public AguardeEstado()
        {
            this.Visivel = ControladorBase.aguardeForm.Visible;
            this.PodeMostrar = AguardeForm.PodeMostrar;
            this.PodeOcultar = AguardeForm.PodeOcultar;
        }

        public AguardeEstado(AguardeAcaoEnum acao)
        {
            ControladorBase.aguardeForm.Parent = null;
            this.Visivel = ControladorBase.aguardeForm.Visible;
            this.PodeMostrar = AguardeForm.PodeMostrar;
            this.PodeOcultar = AguardeForm.PodeOcultar;

            if (acao == AguardeAcaoEnum.Mostrar && !this.Visivel)
            {
                //AguardeForm.PodeMostrar = true;
                ControladorBase.aguardeForm.Mostrar();
            }

            if (acao == AguardeAcaoEnum.Ocultar && this.Visivel)
            {
                //AguardeForm.PodeOcultar = true;
                ControladorBase.aguardeForm.Ocultar();
            }

        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    AguardeForm.PodeMostrar = this.PodeMostrar;
                    AguardeForm.PodeOcultar = this.PodeOcultar;

                    if (ControladorBase.aguardeForm.Visible && !this.Visivel)
                    {
                        ControladorBase.aguardeForm.Ocultar();
                    }

                    if (!ControladorBase.aguardeForm.Visible && this.Visivel)
                    {
                        ControladorBase.aguardeForm.Mostrar();
                    }

                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}
