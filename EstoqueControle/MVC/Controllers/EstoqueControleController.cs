﻿using System.Reflection;

namespace EstoqueControle.MVC.Controllers
{
    public class EstoqueControleController: ControladorBase
    {

        public static void EstoqueControleAction()
        {
            ExecutarAction(MethodBase.GetCurrentMethod().Name);
        }

    }
}
