﻿using EstoqueControle.MVC.Views;

namespace EstoqueControle.MVC.Controllers.Actions
{
    public class EstoqueControleAction : ActionBase
    {
        public void Execute()
        {
            using (ControleEstoqueForm form = new ControleEstoqueForm())
            {
                ControladorBase.ShowModalForm(form);
            }
        }
    }
}