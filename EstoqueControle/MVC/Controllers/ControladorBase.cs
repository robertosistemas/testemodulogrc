﻿using EstoqueControle.MVC.Views;
using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace EstoqueControle.MVC.Controllers
{
    public class ExecutarObj
    {
        public string ActionName;
        public object[] Parametros;
    }

    public class ControladorBase
    {

        private static AguardeForm _aguardeForm = new AguardeForm();

        public static AguardeForm aguardeForm
        {
            get
            {
                if (_aguardeForm == null)
                {
                    _aguardeForm = new AguardeForm();
                }
                return _aguardeForm;
            }
        }

        public static void MostrarAguarde()
        {
            aguardeForm.Mostrar();
        }

        public static void OcultarAguarde()
        {
            aguardeForm.Ocultar();
        }

        public static DialogResult ShowModalForm(Form frm)
        {
            ModalForm frmModal = new ModalForm(frm);
            return frmModal.DialogResult;
        }

        private static string ExecuteMethodName = "Execute";

        private static Assembly assembly = null;

        public static void ExecutarAction(string actionName, params object[] parametros)
        {

            if (assembly == null)
            {
                assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(p => p.GetName().ToString().Contains("EstoqueControle"));
            }

            if (!(assembly == null))
            {

                Type type = assembly.GetType(string.Format("EstoqueControle.MVC.Controllers.Actions.{0}", actionName));

                if (type != null)
                {

                    MethodInfo methodInfo = type.GetMethod(ExecuteMethodName);

                    if (methodInfo != null)
                    {

                        MostrarAguarde();
                        try
                        {

                            object result = null;
                            object classInstance = Activator.CreateInstance(type, null);
                            ParameterInfo[] parameters = methodInfo.GetParameters();

                            if (parameters.Length == 0)
                            {
                                result = methodInfo.Invoke(classInstance, null);
                            }
                            else
                            {
                                result = methodInfo.Invoke(classInstance, parametros);
                            }

                        }
                        finally
                        {
                            OcultarAguarde();
                        }

                    }
                }
            }
        }
    }
}