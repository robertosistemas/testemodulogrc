﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EstoqueControle.PrecoPropostoServico {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="PrecoPropostoServico.IPrecoPropostoServico")]
    public interface IPrecoPropostoServico {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/incluir", ReplyAction="http://tempuri.org/IPrecoPropostoServico/incluirResponse")]
        void incluir(EstoqueModelo.PrecoProposto entidade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/incluir", ReplyAction="http://tempuri.org/IPrecoPropostoServico/incluirResponse")]
        System.Threading.Tasks.Task incluirAsync(EstoqueModelo.PrecoProposto entidade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/atualizar", ReplyAction="http://tempuri.org/IPrecoPropostoServico/atualizarResponse")]
        void atualizar(EstoqueModelo.PrecoProposto entidade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/atualizar", ReplyAction="http://tempuri.org/IPrecoPropostoServico/atualizarResponse")]
        System.Threading.Tasks.Task atualizarAsync(EstoqueModelo.PrecoProposto entidade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/excluir", ReplyAction="http://tempuri.org/IPrecoPropostoServico/excluirResponse")]
        void excluir(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/excluir", ReplyAction="http://tempuri.org/IPrecoPropostoServico/excluirResponse")]
        System.Threading.Tasks.Task excluirAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterPorId", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterPorIdResponse")]
        EstoqueModelo.PrecoProposto obterPorId(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterPorId", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterPorIdResponse")]
        System.Threading.Tasks.Task<EstoqueModelo.PrecoProposto> obterPorIdAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterTodos", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterTodosResponse")]
        System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterTodos();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterTodos", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterTodosResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterTodosAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterPorBicicletaId", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterPorBicicletaIdResponse")]
        System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterPorBicicletaId(int bicicletaId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterPorBicicletaId", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterPorBicicletaIdResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterPorBicicletaIdAsync(int bicicletaId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterPorGerenteEstoqueId", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterPorGerenteEstoqueIdResponse")]
        System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterPorGerenteEstoqueId(int gerenteEstoqueId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterPorGerenteEstoqueId", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterPorGerenteEstoqueIdResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterPorGerenteEstoqueIdAsync(int gerenteEstoqueId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterPorGerenteLojaOnlineId", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterPorGerenteLojaOnlineIdResponse")]
        System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterPorGerenteLojaOnlineId(int gerenteLojaOnlineId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterPorGerenteLojaOnlineId", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterPorGerenteLojaOnlineIdResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterPorGerenteLojaOnlineIdAsync(int gerenteLojaOnlineId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterRegistroAgendar", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterRegistroAgendarResponse")]
        System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterRegistroAgendar();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterRegistroAgendar", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterRegistroAgendarResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterRegistroAgendarAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterRegistroProcessar", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterRegistroProcessarResponse")]
        System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterRegistroProcessar();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPrecoPropostoServico/obterRegistroProcessar", ReplyAction="http://tempuri.org/IPrecoPropostoServico/obterRegistroProcessarResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterRegistroProcessarAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IPrecoPropostoServicoChannel : EstoqueControle.PrecoPropostoServico.IPrecoPropostoServico, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PrecoPropostoServicoClient : System.ServiceModel.ClientBase<EstoqueControle.PrecoPropostoServico.IPrecoPropostoServico>, EstoqueControle.PrecoPropostoServico.IPrecoPropostoServico {
        
        public PrecoPropostoServicoClient() {
        }
        
        public PrecoPropostoServicoClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PrecoPropostoServicoClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PrecoPropostoServicoClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PrecoPropostoServicoClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void incluir(EstoqueModelo.PrecoProposto entidade) {
            base.Channel.incluir(entidade);
        }
        
        public System.Threading.Tasks.Task incluirAsync(EstoqueModelo.PrecoProposto entidade) {
            return base.Channel.incluirAsync(entidade);
        }
        
        public void atualizar(EstoqueModelo.PrecoProposto entidade) {
            base.Channel.atualizar(entidade);
        }
        
        public System.Threading.Tasks.Task atualizarAsync(EstoqueModelo.PrecoProposto entidade) {
            return base.Channel.atualizarAsync(entidade);
        }
        
        public void excluir(int id) {
            base.Channel.excluir(id);
        }
        
        public System.Threading.Tasks.Task excluirAsync(int id) {
            return base.Channel.excluirAsync(id);
        }
        
        public EstoqueModelo.PrecoProposto obterPorId(int id) {
            return base.Channel.obterPorId(id);
        }
        
        public System.Threading.Tasks.Task<EstoqueModelo.PrecoProposto> obterPorIdAsync(int id) {
            return base.Channel.obterPorIdAsync(id);
        }
        
        public System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterTodos() {
            return base.Channel.obterTodos();
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterTodosAsync() {
            return base.Channel.obterTodosAsync();
        }
        
        public System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterPorBicicletaId(int bicicletaId) {
            return base.Channel.obterPorBicicletaId(bicicletaId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterPorBicicletaIdAsync(int bicicletaId) {
            return base.Channel.obterPorBicicletaIdAsync(bicicletaId);
        }
        
        public System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterPorGerenteEstoqueId(int gerenteEstoqueId) {
            return base.Channel.obterPorGerenteEstoqueId(gerenteEstoqueId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterPorGerenteEstoqueIdAsync(int gerenteEstoqueId) {
            return base.Channel.obterPorGerenteEstoqueIdAsync(gerenteEstoqueId);
        }
        
        public System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterPorGerenteLojaOnlineId(int gerenteLojaOnlineId) {
            return base.Channel.obterPorGerenteLojaOnlineId(gerenteLojaOnlineId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterPorGerenteLojaOnlineIdAsync(int gerenteLojaOnlineId) {
            return base.Channel.obterPorGerenteLojaOnlineIdAsync(gerenteLojaOnlineId);
        }
        
        public System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterRegistroAgendar() {
            return base.Channel.obterRegistroAgendar();
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterRegistroAgendarAsync() {
            return base.Channel.obterRegistroAgendarAsync();
        }
        
        public System.Collections.Generic.List<EstoqueModelo.PrecoProposto> obterRegistroProcessar() {
            return base.Channel.obterRegistroProcessar();
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.PrecoProposto>> obterRegistroProcessarAsync() {
            return base.Channel.obterRegistroProcessarAsync();
        }
    }
}
