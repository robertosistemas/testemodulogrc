﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EstoqueControle.ControleEstoqueServico {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ControleEstoqueServico.IControleEstoqueServico")]
    public interface IControleEstoqueServico {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/incluir", ReplyAction="http://tempuri.org/IControleEstoqueServico/incluirResponse")]
        void incluir(EstoqueModelo.ControleEstoque entidade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/incluir", ReplyAction="http://tempuri.org/IControleEstoqueServico/incluirResponse")]
        System.Threading.Tasks.Task incluirAsync(EstoqueModelo.ControleEstoque entidade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/atualizar", ReplyAction="http://tempuri.org/IControleEstoqueServico/atualizarResponse")]
        void atualizar(EstoqueModelo.ControleEstoque entidade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/atualizar", ReplyAction="http://tempuri.org/IControleEstoqueServico/atualizarResponse")]
        System.Threading.Tasks.Task atualizarAsync(EstoqueModelo.ControleEstoque entidade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/excluir", ReplyAction="http://tempuri.org/IControleEstoqueServico/excluirResponse")]
        void excluir(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/excluir", ReplyAction="http://tempuri.org/IControleEstoqueServico/excluirResponse")]
        System.Threading.Tasks.Task excluirAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterPorId", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterPorIdResponse")]
        EstoqueModelo.ControleEstoque obterPorId(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterPorId", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterPorIdResponse")]
        System.Threading.Tasks.Task<EstoqueModelo.ControleEstoque> obterPorIdAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterTodos", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterTodosResponse")]
        System.Collections.Generic.List<EstoqueModelo.ControleEstoque> obterTodos();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterTodos", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterTodosResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.ControleEstoque>> obterTodosAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterPorBicicletaId", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterPorBicicletaIdResponse")]
        System.Collections.Generic.List<EstoqueModelo.ControleEstoque> obterPorBicicletaId(int bicicletaId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterPorBicicletaId", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterPorBicicletaIdResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.ControleEstoque>> obterPorBicicletaIdAsync(int bicicletaId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterPorPeriodo", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterPorPeriodoResponse")]
        System.Collections.Generic.List<EstoqueModelo.ControleEstoque> obterPorPeriodo(System.DateTime dataInicio, System.DateTime dataFim);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterPorPeriodo", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterPorPeriodoResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.ControleEstoque>> obterPorPeriodoAsync(System.DateTime dataInicio, System.DateTime dataFim);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterPorBicicletaIdPeriodo", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterPorBicicletaIdPeriodoResponse")]
        System.Collections.Generic.List<EstoqueModelo.ControleEstoque> obterPorBicicletaIdPeriodo(int bicicletaId, System.DateTime dataInicio, System.DateTime dataFim);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IControleEstoqueServico/obterPorBicicletaIdPeriodo", ReplyAction="http://tempuri.org/IControleEstoqueServico/obterPorBicicletaIdPeriodoResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.ControleEstoque>> obterPorBicicletaIdPeriodoAsync(int bicicletaId, System.DateTime dataInicio, System.DateTime dataFim);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IControleEstoqueServicoChannel : EstoqueControle.ControleEstoqueServico.IControleEstoqueServico, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ControleEstoqueServicoClient : System.ServiceModel.ClientBase<EstoqueControle.ControleEstoqueServico.IControleEstoqueServico>, EstoqueControle.ControleEstoqueServico.IControleEstoqueServico {
        
        public ControleEstoqueServicoClient() {
        }
        
        public ControleEstoqueServicoClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ControleEstoqueServicoClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ControleEstoqueServicoClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ControleEstoqueServicoClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void incluir(EstoqueModelo.ControleEstoque entidade) {
            base.Channel.incluir(entidade);
        }
        
        public System.Threading.Tasks.Task incluirAsync(EstoqueModelo.ControleEstoque entidade) {
            return base.Channel.incluirAsync(entidade);
        }
        
        public void atualizar(EstoqueModelo.ControleEstoque entidade) {
            base.Channel.atualizar(entidade);
        }
        
        public System.Threading.Tasks.Task atualizarAsync(EstoqueModelo.ControleEstoque entidade) {
            return base.Channel.atualizarAsync(entidade);
        }
        
        public void excluir(int id) {
            base.Channel.excluir(id);
        }
        
        public System.Threading.Tasks.Task excluirAsync(int id) {
            return base.Channel.excluirAsync(id);
        }
        
        public EstoqueModelo.ControleEstoque obterPorId(int id) {
            return base.Channel.obterPorId(id);
        }
        
        public System.Threading.Tasks.Task<EstoqueModelo.ControleEstoque> obterPorIdAsync(int id) {
            return base.Channel.obterPorIdAsync(id);
        }
        
        public System.Collections.Generic.List<EstoqueModelo.ControleEstoque> obterTodos() {
            return base.Channel.obterTodos();
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.ControleEstoque>> obterTodosAsync() {
            return base.Channel.obterTodosAsync();
        }
        
        public System.Collections.Generic.List<EstoqueModelo.ControleEstoque> obterPorBicicletaId(int bicicletaId) {
            return base.Channel.obterPorBicicletaId(bicicletaId);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.ControleEstoque>> obterPorBicicletaIdAsync(int bicicletaId) {
            return base.Channel.obterPorBicicletaIdAsync(bicicletaId);
        }
        
        public System.Collections.Generic.List<EstoqueModelo.ControleEstoque> obterPorPeriodo(System.DateTime dataInicio, System.DateTime dataFim) {
            return base.Channel.obterPorPeriodo(dataInicio, dataFim);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.ControleEstoque>> obterPorPeriodoAsync(System.DateTime dataInicio, System.DateTime dataFim) {
            return base.Channel.obterPorPeriodoAsync(dataInicio, dataFim);
        }
        
        public System.Collections.Generic.List<EstoqueModelo.ControleEstoque> obterPorBicicletaIdPeriodo(int bicicletaId, System.DateTime dataInicio, System.DateTime dataFim) {
            return base.Channel.obterPorBicicletaIdPeriodo(bicicletaId, dataInicio, dataFim);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<EstoqueModelo.ControleEstoque>> obterPorBicicletaIdPeriodoAsync(int bicicletaId, System.DateTime dataInicio, System.DateTime dataFim) {
            return base.Channel.obterPorBicicletaIdPeriodoAsync(bicicletaId, dataInicio, dataFim);
        }
    }
}
