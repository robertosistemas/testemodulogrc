﻿using EstoqueControle.MVC.Views;
using System;
using System.Windows.Forms;

namespace EstoqueControle
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            // Configura a informações da Cultura para pt-BR permitindo a correta formatação e apresentação de dados
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("pt-BR");
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("pt-BR");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
           Application.Run(new ControleEstoqueForm());
        }
    }
}
